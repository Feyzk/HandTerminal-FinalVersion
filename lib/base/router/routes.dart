import 'package:connectivity/connectivity.dart';
import 'package:deneme/views/ChangeIp/change_ip_view.dart';
import 'package:deneme/views/Counting3/cubit/countingthree_cubit.dart';
import 'package:deneme/views/Counting3/view/counting_three_view.dart';
import 'package:deneme/views/Picking3/cubit/pickingthree_cubit.dart';
import 'package:deneme/views/Picking3/view/picking_three_view.dart';
import 'package:deneme/views/Splash/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../views/Acceptance/cubit/acceptance_cubit.dart';
import '../../views/Acceptance/view/acceptance_view_screen.dart';
import '../../views/Acceptance2/cubit/acceptancetwo_cubit.dart';
import '../../views/Acceptance2/view/acceptance_two_view_screen.dart';
import '../../views/Allocation/cubit/allocation_cubit.dart';
import '../../views/Allocation/view/allocation_view.dart';
import '../../views/Carriage/cubit/carriage_cubit.dart';
import '../../views/Carriage/view/carriage_view.dart';
import '../../views/Carriage2/cubit/carriagetwo_cubit.dart';
import '../../views/Carriage2/view/carriage_two_view.dart';
import '../../views/Counting/cubit/counting_cubit.dart';
import '../../views/Counting/view/counting_view.dart';
import '../../views/Counting2/cubit/countingtwo_cubit.dart';
import '../../views/Counting2/view/counting_two_view.dart';
import '../../views/Dummy/cubit/dummy_cubit.dart';
import '../../views/Dummy/view/dummy_view.dart';
import '../../views/Home/language.dart';
import '../../views/Login/cubit/login_cubit.dart';
import '../../views/Login/repository/network_service.dart';
import '../../views/Login/repository/repository.dart';
import '../../views/Login/view/login_view_screen.dart';
import '../../views/Menu/cubit/menu_cubit.dart';
import '../../views/Menu/view/menu_view.dart';
import '../../views/Picking/cubit/picking_cubit.dart';
import '../../views/Picking/view/picking_view.dart';
import '../../views/Picking2/cubit/pickingtwo_cubit.dart';
import '../../views/Picking2/view/picking_two_view.dart';
import '../../views/Shifting/cubit/shifting_cubit.dart';
import '../../views/Shifting/view/shifting_view.dart';
import '../../views/Shipment/cubit/shipment_cubit.dart';
import '../../views/Shipment/view/shipment_view.dart';
import '../../views/Shipment2/cubit/shipmenttwo_cubit.dart';
import '../../views/Shipment2/view/shipment_two_view.dart';
import '../../views/ZifBag/cubit/zifbag_cubit.dart';
import '../../views/ZifBag/view/zifbag_view.dart';
import '../base-view/cubit/internet_cubit.dart';
import '../base-view/cubit/logout_cubit.dart';
import '../base-view/cubit/userstatus_cubit.dart';
import '../consts/route_consts.dart';
import '../not_found_view.dart';

class AppRouter {
  Repository repository;
  LoginCubit loginCubit;
  AcceptancetwoCubit acceptanceTwoCubit;
  AcceptanceCubit acceptanceCubit;
  InternetCubit internetCubit;
  Connectivity connectivity;
  LogoutCubit logoutCubit;
  ShipmentCubit shipmentCubit;
  UserstatusCubit userStatusCubit;
  PickingCubit pickingCubit;
  PickingthreeCubit pickingthreeCubit;
  PickingtwoCubit pickingtwoCubit;

  AppRouter() {
    repository = Repository(networkService: NetworkService());
    loginCubit = LoginCubit(repository: repository);
    acceptanceTwoCubit = AcceptancetwoCubit();
    acceptanceCubit = AcceptanceCubit();
    logoutCubit = LogoutCubit();
    connectivity = Connectivity();
    shipmentCubit = ShipmentCubit();
    userStatusCubit = UserstatusCubit();
    internetCubit = InternetCubit(connectivity: connectivity);
  }

  Route generateRouter(RouteSettings settings) {
    switch (settings.name) {
      case "/":
        return MaterialPageRoute(builder: (_) => SplashScreen());
        break;
      case "/home":
        return MaterialPageRoute(builder: (_) => HomePage());
        break;
      case LOGIN_ROUTE:
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<LoginCubit>(
                create: (_) => LoginCubit(),
              )
            ],
            child: LoginViewScreen(),
          ),
        );

      case MENU_ROUTE:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>(
                create: (_) => LoginCubit(),
              ),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<MenuCubit>(
                create: (_) => MenuCubit(),
              )
            ],
            child: MenuView(
              arguments: arguments,
            ),
          ),
        );

      case ACCEPTANCE_ROUTE:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<AcceptanceCubit>(
                create: (_) => AcceptanceCubit(),
              )
            ],
            child: AcceptanceViewScreen(
              arguments: arguments,
            ),
          ),
        );
        break;
      case ACCEPTANCE_TWO_ROUTE:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<AcceptanceCubit>.value(value: acceptanceCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<AcceptancetwoCubit>(
                create: (BuildContext context) => AcceptancetwoCubit(),
              )
            ],
            child: AcceptanceTwoView(
              arguments: arguments,
            ),
          ),
        );
      case DUMMY_ROUTE:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<DummyCubit>(
                create: (BuildContext context) => DummyCubit(),
              )
            ],
            child: DummyView(
              arguments: arguments,
            ),
          ),
        );

      case SHIPMENT_ONE:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<ShipmentCubit>(
                create: (BuildContext context) => ShipmentCubit(),
              )
            ],
            child: ShipmentView(
              arguments: arguments,
            ),
          ),
        );

      case SHIPMENT_TWO:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<ShipmentCubit>.value(value: shipmentCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<ShipmenttwoCubit>(
                create: (BuildContext context) => ShipmenttwoCubit(),
              )
            ],
            child: ShipmentTwoView(
              arguments: arguments,
            ),
          ),
        );
      case PICKING:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<PickingCubit>(
                create: (BuildContext context) => PickingCubit(),
              )
            ],
            child: PickingView(
              arguments: arguments,
            ),
          ),
        );

      case PICKING_TWO:
        final arguments = settings.arguments;

        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<PickingCubit>(
                create: (BuildContext context) => PickingCubit(),
              ),
              BlocProvider<PickingtwoCubit>(
                create: (BuildContext context) => PickingtwoCubit(),
              )
            ],
            child: PickingTwoView(arguments: arguments),
          ),
        );

      case ALLOCATION:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<PickingtwoCubit>(
                create: (BuildContext context) => PickingtwoCubit(),
              ),
              BlocProvider<PickingCubit>(
                create: (BuildContext context) => PickingCubit(),
              ),
              BlocProvider<PickingthreeCubit>(
                create: (BuildContext context) => PickingthreeCubit(),
              ),
              BlocProvider<AllocationCubit>(
                create: (BuildContext context) => AllocationCubit(),
              )
            ],
            child: AllocationView(
              arguments: arguments,
            ),
          ),
        );
      case CARRIAGE:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<CarriageCubit>(
                create: (BuildContext context) => CarriageCubit(),
              )
            ],
            child: CarriageView(
              arguments: arguments,
            ),
          ),
        );
      case CARRIAGE_TWO:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<CarriagetwoCubit>(
                create: (BuildContext context) => CarriagetwoCubit(),
              )
            ],
            child: CarriageTwoView(
              arguments: arguments,
            ),
          ),
        );

      case SHIFTING:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<ShiftingCubit>(
                create: (BuildContext context) => ShiftingCubit(),
              )
            ],
            child: ShiftingView(
              arguments: arguments,
            ),
          ),
        );

      case COUNTING:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<CountingCubit>(
                create: (BuildContext context) => CountingCubit(),
              )
            ],
            child: CountingView(
              arguments: arguments,
            ),
          ),
        );

      case COUNTING2:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<CountingtwoCubit>(
                create: (BuildContext context) => CountingtwoCubit(),
              )
            ],
            child: CountingTwoView(
              arguments: arguments,
            ),
          ),
        );

      case COUNTING3:
        final arguments = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(
                  providers: [
                    BlocProvider<LoginCubit>.value(value: loginCubit),
                    BlocProvider<LogoutCubit>.value(value: logoutCubit),
                    BlocProvider<InternetCubit>.value(value: internetCubit),
                    BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
                    BlocProvider<CountingthreeCubit>(
                      create: (BuildContext context) => CountingthreeCubit(),
                    )
                  ],
                  child: CountingThreeView(
                    arguments: arguments,
                  ),
                ));

      case ZIFBAG:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<ZifbagCubit>(
                create: (BuildContext context) => ZifbagCubit(),
              )
            ],
            child: ZifbagView(
              arguments: arguments,
            ),
          ),
        );

      case PICKING_THREE:
        final arguments = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
              BlocProvider<PickingtwoCubit>(
                create: (BuildContext context) => PickingtwoCubit(),
              ),
              BlocProvider<PickingCubit>(
                create: (BuildContext context) => PickingCubit(),
              ),
              BlocProvider<PickingthreeCubit>(
                create: (BuildContext context) => PickingthreeCubit(),
              )
            ],
            child: PickingThreeView(
              arguments: arguments,
            ),
          ),
        );

      case CHANGE_IP:
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginCubit>.value(value: loginCubit),
              BlocProvider<LogoutCubit>.value(value: logoutCubit),
              BlocProvider<InternetCubit>.value(value: internetCubit),
              BlocProvider<UserstatusCubit>.value(value: userStatusCubit),
            ],
            child: ChangeIpView(),
          ),
        );

      case NOT_FOUND:
        return MaterialPageRoute(builder: (_) => NotFoundView());
      default:
        return null;
    }
  }
}
