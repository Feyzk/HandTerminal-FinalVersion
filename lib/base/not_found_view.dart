import 'package:flutter/material.dart';

class NotFoundView extends StatefulWidget {
  @override
  _NotFoundViewState createState() => _NotFoundViewState();
}

class _NotFoundViewState extends State<NotFoundView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(child: Text('Not Found')),
    );
  }
}
