import 'package:flutter/material.dart';

class CustomMenuItem extends StatelessWidget {
  final String title;
  final String route;

  const CustomMenuItem({Key key, this.title, this.route}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, route);
        },
        child: ListTile(
          title: Text(title),
        ),
      ),
    );
  }
}
