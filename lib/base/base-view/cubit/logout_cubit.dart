import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../consts/uri_consts.dart';
import '../model/logout_response_model.dart';

part 'logout_state.dart';

class LogoutCubit extends Cubit<LogoutState> {
  LogoutCubit() : super(LogoutInitial());

  bool logoutStatus = false;

  // ignore: missing_return
  Future<LogoutResponseModel> logout({String username}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/auth/logout'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{'username': username}),
    );

    if (response.statusCode == HttpStatus.ok) {
      final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      await sharedPreferences.clear();
      logoutStatus = true;
      emit(LogoutSuccess());
      logoutStatus = false;
    } else {
      logoutStatus = false;
      emit(LogoutFailed('Logout Failed'));
    }
  }
}
