import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../consts/uri_consts.dart';
import '../model/user_status_response.dart';

part 'userstatus_state.dart';

class UserstatusCubit extends Cubit<UserstatusState> {
  UserstatusCubit() : super(UserstatusInitial());

  bool userStatus = true;

  String email;

  // ignore: missing_return
  Future<bool> userStatusControl({String username}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/auth/status_control'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          'username': username,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = UserStatusResponse.fromJson(jsonDecode(response.body));
      this.userStatus = jsonBody.status;
    }
  }
}
