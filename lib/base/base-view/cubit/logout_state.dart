part of 'logout_cubit.dart';

@immutable
abstract class LogoutState {}

class LogoutInitial extends LogoutState {}

class LogoutSuccess extends LogoutState {}

class LogoutFailed extends LogoutState {
  final String message;

  LogoutFailed(this.message);
}
