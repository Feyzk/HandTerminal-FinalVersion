import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../generated/l10n.dart';
import '../consts/route_consts.dart';
import 'cubit/internet_cubit.dart';
import 'cubit/logout_cubit.dart';
import 'cubit/userstatus_cubit.dart';

class BaseView extends StatefulWidget {
  const BaseView({
    Key key,
    this.home,
    this.appBarTitle,
    this.username,
    this.leadingFunction,
    this.finishLeading,
    this.leading,
  }) : super(key: key);

  @override
  _BaseViewState createState() => _BaseViewState();
  final Widget home;
  final String appBarTitle;
  final String username;
  final VoidCallback leadingFunction;
  final dynamic finishLeading;
  final Widget leading;
}

class _BaseViewState extends State<BaseView> {
  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    final _logoutCubit = BlocProvider.of<LogoutCubit>(context);
    final _userStatusCubit = BlocProvider.of<UserstatusCubit>(context);
    final S delegate = S.of(context);

    _userStatusCubit.userStatusControl(username: widget.username);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        key: _scaffoldKey,
        title: Text(
          widget.appBarTitle,
          style: TextStyle(
            fontSize: 15,
          ),
        ),
        leading: widget.leading ?? widget.leading,
        actions: [
          widget.finishLeading == null ? Text("") : widget.finishLeading,
          Opacity(
            opacity: 0,
            child: Text("asda"),
          ),
          TextButton(
            onPressed: () async {
              _logoutCubit.logout(username: widget.username).whenComplete(
                    () => {
                      _userStatusCubit.userStatus = true,
                      Navigator.of(context).pushNamedAndRemoveUntil(LOGIN_ROUTE, (route) => false),
                    },
                  );
            },
            child: Text(
              delegate.logout,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontSize: 15,
              ),
            ),
          ),
        ],
      ),
      body: BlocBuilder<InternetCubit, InternetState>(
        builder: (context, state) {
          if (state is InternetConnected) {
            if (_logoutCubit.logoutStatus == false && _userStatusCubit.userStatus == true) {
              return widget.home;
            } else {
              return Center(
                child: Text(delegate.undefinedUser),
              );
            }
          } else {
            return Center(
              child: Text(delegate.internetError),
            );
          }
        },
      ),
    );
  }
}
