import 'dart:convert';

LogoutResponseModel logoutResponseModelFromJson(String str) =>
    LogoutResponseModel.fromJson(json.decode(str));

String logoutResponseModelToJson(LogoutResponseModel data) =>
    json.encode(data.toJson());

class LogoutResponseModel {
  LogoutResponseModel({
    this.message,
    this.username,
  });

  String message;
  String username;

  factory LogoutResponseModel.fromJson(Map<String, dynamic> json) =>
      LogoutResponseModel(
        message: json["message"],
        username: json["username"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "username": username,
      };
}
