import 'dart:convert';

UserStatusResponse userStatusResponseFromJson(String str) =>
    UserStatusResponse.fromJson(json.decode(str));

String userStatusResponseToJson(UserStatusResponse data) =>
    json.encode(data.toJson());

class UserStatusResponse {
  UserStatusResponse({
    this.status,
  });

  bool status;

  factory UserStatusResponse.fromJson(Map<String, dynamic> json) =>
      UserStatusResponse(
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
      };
}
