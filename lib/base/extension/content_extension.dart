import 'package:flutter/material.dart';

extension ContentExtension on BuildContext {
  double dynamicWidth(double val) => MediaQuery.of(this).size.width * val;
  double dynamicHeigt(double val) => MediaQuery.of(this).size.height * val;

  ThemeData get theme => Theme.of(this);
}
