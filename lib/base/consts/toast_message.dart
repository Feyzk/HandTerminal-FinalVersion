import 'package:flutter/material.dart';

class ToastMessage {
  void showToast(BuildContext context, String message) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        backgroundColor: Colors.red,
        elevation: 10,
        content: Text(
          message,
          style: TextStyle(color: Colors.white, fontSize: 24),
        ),
        action: SnackBarAction(textColor: Colors.white, label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }
}
