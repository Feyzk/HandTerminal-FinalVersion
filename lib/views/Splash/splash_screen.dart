import 'dart:async';

import 'package:deneme/base/base-view/base_view.dart';
import 'package:deneme/base/consts/route_consts.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String finalUsername;
  List finalRoles;

  @override
  void initState() {
    WidgetsFlutterBinding.ensureInitialized();
    getValidationData().whenComplete(
      () async {
        Timer(
            Duration(seconds: 2),
            () => Navigator.pushNamedAndRemoveUntil(
                context,
                finalUsername == null ? HOME_ROUTE : MENU_ROUTE,
                (route) => false,
                arguments: [finalRoles, finalUsername]));
      },
    );
    super.initState();
  }

  Future getValidationData() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var username = sharedPreferences.getString('username');
    var roles = sharedPreferences
        .getStringList('roles')
        .map((e) => int.parse(e))
        .toList();

    setState(() {
      finalUsername = username;
      finalRoles = roles;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(),
    );
  }
}
