import 'package:deneme/base/consts/route_consts.dart';
import 'package:deneme/base/consts/toast_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../../Login/cubit/login_cubit.dart';
import '../cubit/allocation_cubit.dart';

class AllocationView extends StatefulWidget {
  final List arguments;

  const AllocationView({Key key, this.arguments}) : super(key: key);
  @override
  _AllocationViewState createState() => _AllocationViewState();
}

class _AllocationViewState extends State<AllocationView> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _textController;
  FocusNode _focusNode;
  ToastMessage _toastMessage = ToastMessage();

  @override
  void initState() {
    _textController = TextEditingController();
    _focusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final _loginCubit = BlocProvider.of<LoginCubit>(context);
    final _allocationCubit = BlocProvider.of<AllocationCubit>(context);

    return BaseView(
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios_outlined,
        ),
        onPressed: () {
          Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0][0]);
        },
      ),
      finishLeading: TextButton(
        child: Text(delegate.finish),
        onPressed: () {
          _allocationCubit
              .finish(
                username: widget.arguments[0][0][1],
                pickingCarId: widget.arguments[1],
                pickingOrderId: widget.arguments[0][1],
              )
              .then(
                (response) => {
                  if (response == true)
                    {
                      Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0][0]),
                    }
                },
              );
        },
      ),
      username: widget.arguments[1],
      appBarTitle: delegate.allocation,
      home: BlocConsumer<AllocationCubit, AllocationState>(
        listener: (context, state) {
          if (state is AllocationError) {
            String message = state.message;
            if (message == "notLoadedCar") {
              _toastMessage.showToast(context, delegate.notLoadedCar);
            } else {
              _toastMessage.showToast(context, delegate.badRequestError);
            }
          }
        },
        // ignore: missing_return
        builder: (context, state) {
          if (state is AllocationInitial) {
            _allocationCubit.getAllocationList(pickingOrderId: widget.arguments[0][1]);
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is AllocationList) {
            return Padding(
              padding: EdgeInsets.symmetric(
                horizontal: context.dynamicWidth(0.1),
                vertical: context.dynamicWidth(0.1),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    flex: 2,
                    child: buildAllocationBarcodeForm(delegate, _allocationCubit, _loginCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: buildStoreNameField(_allocationCubit, delegate),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: Center(
                      child: Text(
                        _allocationCubit.productList.length.toString(),
                        style: TextStyle(fontSize: 22),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 13,
                    child: buildProductList(_allocationCubit),
                  ),
                ],
              ),
            );
          } else {
            return Padding(
              padding: EdgeInsets.symmetric(
                horizontal: context.dynamicWidth(0.1),
                vertical: context.dynamicWidth(0.1),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    flex: 2,
                    child: buildAllocationBarcodeForm(delegate, _allocationCubit, _loginCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: buildStoreNameField(_allocationCubit, delegate),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: Center(
                      child: Text(
                        _allocationCubit.productList.length.toString(),
                        style: TextStyle(fontSize: 22),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 13,
                    child: buildProductList(_allocationCubit),
                  ),
                ],
              ),
            );
          }
        },
      ),
    );
  }

  ListView buildProductList(AllocationCubit _allocationCubit) {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(color: Colors.white60),
      itemCount: _allocationCubit.productList.length,
      itemBuilder: (context, index) {
        if (_allocationCubit.parameterResult == "1") {
          return Dismissible(
            onDismissed: (direction) {
              _allocationCubit
                  .delete(
                username: widget.arguments[0][0][1],
                barcode: _allocationCubit.productList[index],
                pickingCarId: widget.arguments[1],
                pickingOrderId: widget.arguments[0][1],
                index: index,
                crossDockId: _allocationCubit.crossDockList[index],
              )
                  .then((value) {
                _allocationCubit.deleteItem(index);
              });
            },
            key: UniqueKey(),
            child: ListTile(
                leading: Icon(Icons.arrow_forward_ios_outlined),
                title: Text(
                  _allocationCubit.productList[index],
                ),
                subtitle:
                    Text('Store Name: ${_allocationCubit.storeNameList[index]}' + '/' + 'Store Total: ${_allocationCubit.storeTotalList[index]}')),
          );
        } else {
          return ListTile(
            leading: Icon(Icons.arrow_forward_ios_outlined),
            title: Text(
              _allocationCubit.productList[index],
            ),
            subtitle: Text('Store Name: ${_allocationCubit.storeNameList[index]}' + '/' + 'Store Total: ${_allocationCubit.storeTotalList[index]}'),
          );
        }
      },
    );
  }

  Center buildStoreNameField(AllocationCubit _allocationCubit, S delegate) {
    return Center(
      child: SizedBox(
        width: context.dynamicWidth(0.3),
        height: context.dynamicHeigt(0.05),
        child: Text(
          _allocationCubit.storeName,
          style: TextStyle(fontSize: 22),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Form buildAllocationBarcodeForm(S delegate, AllocationCubit _allocationCubit, LoginCubit _loginCubit) {
    return Form(
      key: _formKey,
      child: TextFormField(
        focusNode: _focusNode,
        autofocus: true,
        controller: _textController,
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          _focusNode.requestFocus();
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            _allocationCubit
                .readAllocationBarcode(
                  username: widget.arguments[0][0][1],
                  barcode: value,
                  pickingCarId: widget.arguments[1],
                  pickingOrderId: widget.arguments[0][1],
                )
                .then((value) => {
                      _allocationCubit.getAllocationList(
                        pickingOrderId: widget.arguments[0][1],
                      )
                    })
                .then((value) {
              _textController.clear();
            });
          }
        },
        validator: (value) {
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            return null;
          }
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
