import 'dart:convert';

ReadAllocationBarcodeResponse readAllocationBarcodeResponseFromJson(String str) => ReadAllocationBarcodeResponse.fromJson(json.decode(str));

String readAllocationBarcodeResponseToJson(ReadAllocationBarcodeResponse data) => json.encode(data.toJson());

class ReadAllocationBarcodeResponse {
  ReadAllocationBarcodeResponse({
    this.read,
    this.storeName,
    this.message,
    this.parameterResult,
    this.crossDockId,
  });

  bool read;
  String storeName;
  String message;
  String parameterResult;
  String crossDockId;

  factory ReadAllocationBarcodeResponse.fromJson(Map<String, dynamic> json) => ReadAllocationBarcodeResponse(
        read: json["read"],
        storeName: json["Store_Name"],
        message: json["message"],
        parameterResult: json["parameter_result"],
        crossDockId: json["Cross_Dock_ID"],
      );

  Map<String, dynamic> toJson() => {
        "read": read,
        "Store_Name": storeName,
        "message": message,
        "parameter_result": parameterResult,
        "Cross_Dock_ID": crossDockId,
      };
}
