import 'dart:convert';

List<GetAllocationResponse> getAllocationResponseFromJson(String str) =>
    List<GetAllocationResponse>.from(json.decode(str).map((x) => GetAllocationResponse.fromJson(x)));

String getAllocationResponseToJson(List<GetAllocationResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetAllocationResponse {
  GetAllocationResponse({
    this.barcode,
    this.storeName,
    this.crossDockId,
    this.storeTotal,
    this.parameterResult,
  });

  String barcode;
  String storeName;
  String crossDockId;
  String storeTotal;
  String parameterResult;

  factory GetAllocationResponse.fromJson(Map<String, dynamic> json) => GetAllocationResponse(
        barcode: json["Barcode"],
        storeName: json["Store_Name"],
        crossDockId: json["Cross_Dock_ID"],
        storeTotal: json["Store_Total"],
        parameterResult: json["parameter_result"],
      );

  Map<String, dynamic> toJson() => {
        "Barcode": barcode,
        "Store_Name": storeName,
        "Cross_Dock_ID": crossDockId,
        "Store_Total": storeTotal,
        "parameter_result": parameterResult,
      };
}
