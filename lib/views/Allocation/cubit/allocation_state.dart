part of 'allocation_cubit.dart';

@immutable
abstract class AllocationState {}

class AllocationInitial extends AllocationState {}

class AllocationError extends AllocationState {
  final String message;

  AllocationError(this.message);
}

class AllocationList extends AllocationState {}
