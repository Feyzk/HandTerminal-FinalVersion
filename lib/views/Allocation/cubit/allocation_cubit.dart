import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:deneme/views/Allocation/model/get_allocation_response.dart';
import '../../../base/consts/uri_consts.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../model/allocation_barcode_response_model.dart';

part 'allocation_state.dart';

class AllocationCubit extends Cubit<AllocationState> {
  AllocationCubit() : super(AllocationInitial());

  List productList = [];
  List storeNameList = [];
  List storeTotalList = [];
  List crossDockList = [];
  String storeName = "";
  String errorMessage;
  String parameterResult;

  void addItem(String item) {
    productList.insert(0, item);
    emit(AllocationList());
  }

  void deleteItem(int index) {
    productList.removeAt(index);
    emit(AllocationList());
  }

  void clearLists() {
    productList.clear();
    storeNameList.clear();
    storeTotalList.clear();
    crossDockList.clear();
  }

  // ignore: missing_return
  Future<ReadAllocationBarcodeResponse> readAllocationBarcode({
    String username,
    String barcode,
    String pickingCarId,
    String pickingOrderId,
  }) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/picking/read_allocation'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "username": username,
        "barcode": barcode,
        "Picking_Car_ID": pickingCarId,
        "Picking_Order_ID": pickingOrderId,
      }),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = ReadAllocationBarcodeResponse.fromJson(jsonDecode(response.body));
      this.storeName = responseBody.storeName;
      this.parameterResult = responseBody.parameterResult;
      addItem(barcode);
    } else if (response.statusCode == HttpStatus.badRequest) {
      var responseBody = ReadAllocationBarcodeResponse.fromJson(jsonDecode(response.body));
      this.parameterResult = responseBody.parameterResult;
      addItem(barcode);
      this.storeName = "SHELF";
    } else if (response.statusCode == HttpStatus.unauthorized) {
      emit(AllocationError('notLoadedCar'));
    } else {
      emit(AllocationError('badRequestError'));
    }
  }

  Future<GetAllocationResponse> getAllocationList({String pickingOrderId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/picking/get_allocation'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{"Picking_Order_ID": pickingOrderId}),
    );
    if (response.statusCode == HttpStatus.ok) {
      emit(AllocationList());
      var jsonBody = (jsonDecode(response.body)) as List;
      var jsonList = jsonBody.map((e) => GetAllocationResponse.fromJson(e)).toList();
      this.parameterResult = jsonList[0].parameterResult;
      clearLists();
      for (var i = 0; i < jsonList.length; i++) {
        productList.insert(0, jsonList[i].barcode);
        storeNameList.insert(0, jsonList[i].storeName);
        storeTotalList.insert(0, jsonList[i].storeTotal);
        crossDockList.insert(0, jsonList[i].crossDockId);
      }
    }
  }

  Future<bool> finish({
    String username,
    String pickingCarId,
    String pickingOrderId,
  }) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/picking/finish'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          "username": username,
          "Picking_Car_ID": pickingCarId,
          "Picking_Order_ID": pickingOrderId,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> delete({
    String username,
    String pickingCarId,
    String pickingOrderId,
    String barcode,
    String crossDockId,
    int index,
  }) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/picking/delete_allocation'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          "username": username,
          "Picking_Car_ID": pickingCarId,
          "Picking_Order_ID": pickingOrderId,
          "barcode": barcode,
          "Cross_Dock_ID": crossDockId,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      return true;
    } else {
      return false;
    }
  }
}
