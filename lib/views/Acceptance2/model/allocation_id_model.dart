// To parse this JSON data, do
//
//     final allocationIdModel = allocationIdModelFromJson(jsonString);

import 'dart:convert';

AllocationIdModel allocationIdModelFromJson(String str) => AllocationIdModel.fromJson(json.decode(str));

String allocationIdModelToJson(AllocationIdModel data) => json.encode(data.toJson());

class AllocationIdModel {
  AllocationIdModel({
    this.allocationId,
    this.invoiceAmount,
    this.acceptedTotal,
    this.parameterResult,
  });

  String allocationId;
  String invoiceAmount;
  int acceptedTotal;
  String parameterResult;

  factory AllocationIdModel.fromJson(Map<String, dynamic> json) => AllocationIdModel(
        allocationId: json["allocation_id"],
        invoiceAmount: json["invoice_amount"],
        acceptedTotal: json["acceptedTotal"],
        parameterResult: json["parameter_result"],
      );

  Map<String, dynamic> toJson() => {
        "allocation_id": allocationId,
        "invoice_amount": invoiceAmount,
        "acceptedTotal": acceptedTotal,
        "parameter_result": parameterResult,
      };
}
