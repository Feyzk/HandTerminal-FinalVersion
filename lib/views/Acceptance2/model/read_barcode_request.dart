import 'dart:convert';

ReadBarcodeRequest readBarcodeRequestFromJson(String str) =>
    ReadBarcodeRequest.fromJson(json.decode(str));

String readBarcodeRequestToJson(ReadBarcodeRequest data) =>
    json.encode(data.toJson());

class ReadBarcodeRequest {
  ReadBarcodeRequest({
    this.username,
    this.acceptanceId,
    this.allocationId,
    this.barcode,
  });

  String username;
  String acceptanceId;
  String allocationId;
  String barcode;

  factory ReadBarcodeRequest.fromJson(Map<String, dynamic> json) =>
      ReadBarcodeRequest(
        username: json["username"],
        acceptanceId: json["acceptance_id"],
        allocationId: json["allocation_id"],
        barcode: json["barcode"],
      );

  Map<String, dynamic> toJson() => {
        "username": username,
        "acceptance_id": acceptanceId,
        "allocation_id": allocationId,
        "barcode": barcode,
      };
}
