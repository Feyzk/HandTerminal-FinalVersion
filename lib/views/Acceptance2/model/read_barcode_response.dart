import 'dart:convert';

ReadBarcodeResponse readBarcodeResponseFromJson(String str) =>
    ReadBarcodeResponse.fromJson(json.decode(str));

String readBarcodeResponseToJson(ReadBarcodeResponse data) =>
    json.encode(data.toJson());

class ReadBarcodeResponse {
  ReadBarcodeResponse({
    this.message,
    this.storeName,
    this.crossDockId,
    this.storeTotal,
  });

  String message;
  String storeName;
  String crossDockId;
  String storeTotal;

  factory ReadBarcodeResponse.fromJson(Map<String, dynamic> json) =>
      ReadBarcodeResponse(
        message: json["message"],
        storeName: json["Store_Name"],
        crossDockId: json["Cross_Dock_ID"],
        storeTotal: json["Store_Total"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "Store_Name": storeName,
        "Cross_Dock_ID": crossDockId,
        "Store_Total": storeTotal,
      };
}
