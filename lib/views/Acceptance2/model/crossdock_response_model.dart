class CrossdockResponseModel {
  String barcode;
  String crossDockID;
  String storeName;
  String storeTotal;
  int acceptedTotal;

  CrossdockResponseModel({this.barcode, this.crossDockID, this.storeName, this.storeTotal, this.acceptedTotal});

  CrossdockResponseModel.fromJson(Map<String, dynamic> json) {
    barcode = json['Barcode'];
    crossDockID = json['Cross_Dock_ID'];
    storeName = json['Store_Name'];
    storeTotal = json['Store_Total'];
    acceptedTotal = json['acceptedTotal'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Barcode'] = this.barcode;
    data['Cross_Dock_ID'] = this.crossDockID;
    data['Store_Name'] = this.storeName;
    data['Store_Total'] = this.storeTotal;
    data['acceptedTotal'] = this.acceptedTotal;
    return data;
  }
}
