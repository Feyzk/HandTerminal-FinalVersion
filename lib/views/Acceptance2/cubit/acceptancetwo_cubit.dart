import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:deneme/views/Acceptance2/model/crossdock_response_model.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';
import '../model/allocation_id_model.dart';
import '../model/read_barcode_response.dart';

part 'acceptancetwo_state.dart';

class AcceptancetwoCubit extends Cubit<AcceptancetwoState> {
  AcceptancetwoCubit() : super(AcceptancetwoInitial());

  List errors = ["badRequestError"];
  List productList = [];
  String allocationId;
  String storeName = "";
  List crossDockIdList = [];
  String invoiceAmount;
  String parameterResult;
  List productStoreName = [];
  List storeTotal = [];
  int productListLength;
  List unvisibleList = [];
  int acceptedTotal = 0;

  void addItem(String item) {
    productList.insert(0, item);
    unvisibleList.insert(0, item);
    crossDockIdList.insert(0, item);
    emit(AcceptancetwoStart());
  }

  void removeItem(int index) {
    productList.removeAt(index);
    crossDockIdList.removeAt(index);
    unvisibleList.removeAt(index);
    productListLength = productListLength - 1;
    acceptedTotal = acceptedTotal - 1;
    emit(AcceptancetwoStart());
  }

  void emitStart() {
    emit(AcceptancetwoStart());
  }

  // ignore: missing_return
  Future<AllocationIdModel> getAllocationId({String acceptanceId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/acceptance/2/get_allocation'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{'acceptance_id': acceptanceId},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = AllocationIdModel.fromJson(jsonDecode(response.body));
      this.allocationId = responseBody.allocationId;
      this.parameterResult = responseBody.parameterResult;
      this.invoiceAmount = responseBody.invoiceAmount;
      productListLength = responseBody.acceptedTotal;
      emit(AcceptancetwoStart());
    } else {
      emit(AcceptancetwoError(errors[0]));
    }
  }

  // ignore: missing_return
  Future<ReadBarcodeResponse> readBarcode({String username, String acceptanceId, String allocationId, String barcode}) async {
    final response = await http.post(Uri.http(UriConst.uri, 'api/acceptance/2/read_barcode'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{"username": username, "acceptance_id": acceptanceId, "allocation_id": allocationId, "barcode": barcode}));
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = ReadBarcodeResponse.fromJson(jsonDecode(response.body));
      this.storeName = responseBody.storeName;
      this.productStoreName.insert(0, responseBody.storeName);
      this.storeTotal.insert(0, responseBody.storeTotal);
      this.unvisibleList.insert(0, responseBody.storeName == "Jane Doe" ? "Jane Doe" : barcode);
      this.productList.insert(0, barcode);
      this.crossDockIdList.insert(0, (responseBody.crossDockId == null ? "" : responseBody.crossDockId));

      emit(AcceptancetwoStart());
    } else {
      emit(AcceptancetwoError(errors[0]));
    }
  }

  Future<dynamic> finishBarcode({String username, String acceptanceId, String allocationID}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/acceptance/2/finish'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username, "acceptance_id": acceptanceId, "allocation_id": allocationId},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      emit(AcceptancetwoCompleted());
    } else {
      emit(AcceptancetwoError(errors[0]));
    }
  }

  Future<dynamic> removeBarcode({String username, String acceptanceId, String allocationID, String barcode, String crossDockId, int index}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/acceptance/2/remove_barcode'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          "username": username,
          "acceptance_id": acceptanceId,
          "allocation_id": allocationId,
          "barcode": barcode,
          "Cross_Dock_ID": crossDockId
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      return true;
    } else {
      return false;
    }
  }

  Future<List<CrossdockResponseModel>> getCrossDockList(String acceptanceId) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/acceptance/2/get_crossdock'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"acceptance_id": acceptanceId},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = (jsonDecode(response.body)) as List;
      clearLists();
      var crossDockList = jsonBody.map((e) => CrossdockResponseModel.fromJson(e)).toList();

      for (var i = 0; i < crossDockList.length; i++) {
        this.unvisibleList.insert(i, crossDockList[i].barcode);
        this.productList.insert(i, crossDockList[i].barcode);
        this.crossDockIdList.insert(i, crossDockList[i].crossDockID);
        this.productStoreName.insert(i, crossDockList[i].storeName);
        this.storeTotal.insert(i, crossDockList[i].storeTotal);
        this.acceptedTotal = crossDockList[0].acceptedTotal;
      }
      print(storeTotal);
    } else {}
  }

  void clearLists() {
    unvisibleList.clear();
    productList.clear();
    crossDockIdList.clear();
    productStoreName.clear();
    storeTotal.clear();
  }
}
