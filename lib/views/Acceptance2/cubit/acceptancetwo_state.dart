part of 'acceptancetwo_cubit.dart';

@immutable
abstract class AcceptancetwoState {}

class AcceptancetwoInitial extends AcceptancetwoState {}

class AcceptancetwoStart extends AcceptancetwoState {
  List<CrossdockResponseModel> crossDockList;
  AcceptancetwoStart({
    this.crossDockList,
  });
}

class AcceptancetwoCompleted extends AcceptancetwoState {}

class AcceptancetwoAddItem extends AcceptancetwoState {}

class AcceptancetwoError extends AcceptancetwoState {
  final String message;

  AcceptancetwoError(this.message);
}
