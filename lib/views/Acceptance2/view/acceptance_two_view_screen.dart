import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/route_consts.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/acceptancetwo_cubit.dart';

class AcceptanceTwoView extends StatefulWidget {
  final List arguments;

  const AcceptanceTwoView({Key key, this.arguments}) : super(key: key);
  @override
  _AcceptanceTwoViewState createState() => _AcceptanceTwoViewState();
}

class _AcceptanceTwoViewState extends State<AcceptanceTwoView> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _textController;
  SharedPreferences sharedPreferences;
  FocusNode _focusNode;

  @override
  void initState() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    _textController = TextEditingController();
    _focusNode = FocusNode();
    shared();
    super.initState();
  }

  Future<void> shared() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _textController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final _acceptanceTwoCubit = BlocProvider.of<AcceptancetwoCubit>(context);
    _acceptanceTwoCubit.getAllocationId(acceptanceId: widget.arguments[1]);
    return BaseView(
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios_outlined,
        ),
        onPressed: () {
          Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0]);
        },
      ),
      finishLeading: TextButton(
        child: Text('Finish'),
        onPressed: () {
          _acceptanceTwoCubit.finishBarcode(
              username: widget.arguments[0][1], acceptanceId: widget.arguments[1], allocationID: _acceptanceTwoCubit.allocationId);
        },
      ),
      username: widget.arguments[0][1],
      appBarTitle: delegate.acceptance,
      home: BlocConsumer<AcceptancetwoCubit, AcceptancetwoState>(
        listener: (context, state) {
          if (state is AcceptancetwoInitial) {
            _acceptanceTwoCubit.getCrossDockList(widget.arguments[1]);
          } else if (state is AcceptancetwoCompleted) {
            Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0]);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
        // ignore: missing_return
        builder: (context, state) {
          if (state is AcceptancetwoInitial) {
            _acceptanceTwoCubit.getCrossDockList(widget.arguments[1]);

            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is AcceptancetwoStart) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Text(
                      'Invoice Id : ${widget.arguments[2]}',
                    ),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: buildBarcodeField(delegate, _acceptanceTwoCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: buildStoreNameField(_acceptanceTwoCubit),
                  ),
                  Expanded(
                    flex: 3,
                    child: buildProductCountField(_acceptanceTwoCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 15,
                    child: buildProductList(_acceptanceTwoCubit, state),
                  ),
                ],
              ),
            );
          } else if (state is AcceptancetwoError) {
            return Center(
              child: Text(delegate.badRequestError),
            );
          } else if (state is AcceptancetwoCompleted) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  ListView buildProductList(AcceptancetwoCubit _acceptanceTwoCubit, AcceptancetwoStart state) {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(
        color: Colors.white60,
      ),
      itemCount: _acceptanceTwoCubit.productList.length,
      itemBuilder: (context, index) {
        if (_acceptanceTwoCubit.parameterResult == "1") {
          return Dismissible(
            onDismissed: (direction) {
              _acceptanceTwoCubit
                  .removeBarcode(
                      username: widget.arguments[0][1],
                      acceptanceId: widget.arguments[1],
                      allocationID: _acceptanceTwoCubit.allocationId,
                      barcode: _acceptanceTwoCubit.unvisibleList[index],
                      crossDockId: _acceptanceTwoCubit.crossDockIdList[index],
                      index: index)
                  .then((value) => _acceptanceTwoCubit.removeItem(index))
                  .then((value) => _acceptanceTwoCubit.getCrossDockList(widget.arguments[1]))
                  .then((value) => _acceptanceTwoCubit.emitStart());
            },
            key: UniqueKey(),
            child: ListTile(
              leading: Icon(Icons.arrow_forward_ios_outlined),
              title: Text(
                _acceptanceTwoCubit.productList[index],
              ),
              subtitle:
                  Text('Store Name: ${_acceptanceTwoCubit.productStoreName[index]}' + '/' + 'Store Total: ${_acceptanceTwoCubit.storeTotal[index]}'),
            ),
          );
        } else {
          return ListTile(
            leading: Icon(Icons.arrow_forward_ios_outlined),
            title: Text(
              _acceptanceTwoCubit.productList[index],
            ),
            subtitle:
                Text('Store Name: ${_acceptanceTwoCubit.productStoreName[index]}' + '/' + 'Store Total: ${_acceptanceTwoCubit.storeTotal[index]}'),
          );
        }
      },
    );
  }

  SizedBox buildFinishButton(AcceptancetwoCubit _acceptanceTwoCubit, S delegate) {
    return SizedBox(
      width: context.dynamicWidth(0.8),
      height: context.dynamicHeigt(0.1),
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.black87),
        ),
        onPressed: () {
          _acceptanceTwoCubit.finishBarcode(
              username: widget.arguments[0][1], acceptanceId: widget.arguments[1], allocationID: _acceptanceTwoCubit.allocationId);
        },
        child: Text(delegate.finish),
      ),
    );
  }

  SizedBox buildStoreNameField(AcceptancetwoCubit _acceptanceTwoCubit) {
    return SizedBox(
      width: context.dynamicWidth(0.05),
      height: context.dynamicHeigt(0.05),
      child: Text(
        _acceptanceTwoCubit.storeName,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 20),
      ),
    );
  }

  SizedBox buildProductCountField(AcceptancetwoCubit _acceptanceTwoCubit) {
    return SizedBox(
      width: context.dynamicWidth(0.05),
      height: context.dynamicHeigt(0.05),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            (_acceptanceTwoCubit.acceptedTotal.toString()),
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20),
          ),
          Text(
            ' / ',
            style: TextStyle(fontSize: 20),
          ),
          Text(
            _acceptanceTwoCubit.invoiceAmount,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20),
          ),
        ],
      ),
    );
  }

  Form buildBarcodeField(S delegate, AcceptancetwoCubit _acceptanceTwoCubit) {
    return Form(
      key: _formKey,
      child: TextFormField(
        autofocus: true,
        focusNode: _focusNode,
        controller: _textController,
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          _focusNode.requestFocus();
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            _acceptanceTwoCubit.storeName = "";
            _acceptanceTwoCubit
                .readBarcode(
                    username: widget.arguments[0][1],
                    acceptanceId: widget.arguments[1],
                    allocationId: _acceptanceTwoCubit.allocationId,
                    barcode: value)
                .then(
                  (response) => {
                    _textController.clear(),
                  },
                )
                .then((value) => _acceptanceTwoCubit.acceptedTotal += 1)
                .then((value) => _acceptanceTwoCubit.emitStart());
          }
        },
        validator: (value) {
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            return null;
          }
        },
        decoration: InputDecoration(
          labelText: delegate.productBarcode,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
