import 'package:flutter/services.dart';

import '../../../base/consts/toast_message.dart';
import '../../../base/extension/content_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/route_consts.dart';
import '../../../generated/l10n.dart';
import '../cubit/shipmenttwo_cubit.dart';

class ShipmentTwoView extends StatefulWidget {
  final List arguments;

  const ShipmentTwoView({
    Key key,
    this.arguments,
  }) : super(key: key);
  @override
  _ShipmentTwoViewState createState() => _ShipmentTwoViewState();
}

class _ShipmentTwoViewState extends State<ShipmentTwoView> {
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  TextEditingController _textController;
  TextEditingController _textController2;
  ToastMessage toastMessage = ToastMessage();
  FocusNode _focusNode;
  String locationID;

  @override
  void initState() {
    _textController = TextEditingController();
    _textController2 = TextEditingController();
    _focusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _textController2.dispose();
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final _shipmenttwoCubit = BlocProvider.of<ShipmenttwoCubit>(context);

    return BaseView(
      username: widget.arguments[0][1],
      appBarTitle: delegate.shipmentBag,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios_outlined,
        ),
        onPressed: () {
          Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0]);
        },
      ),
      finishLeading: TextButton(
        child: Text(delegate.closeBag),
        onPressed: () {
          _shipmenttwoCubit.closeBag(username: widget.arguments[0][1], bagID: widget.arguments[1], locationID: locationID);
        },
      ),
      home: BlocConsumer<ShipmenttwoCubit, ShipmenttwoState>(
        listener: (context, state) {
          if (state is ShipmenttwoClosed) {
            Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0]);
          } else if (state is ShipmenttwoError) {
            String message = state.message;
            if (message == "incompatible") {
              message = delegate.incompatible;
            } else if (message == "closedBag") {
              message = delegate.closedBag;
            } else if (message == "notActive") {
              message = delegate.notActive;
            } else if (message == "noLocation") {
              message = delegate.noLocation;
            } else {
              message = delegate.badRequestError;
            }
            FocusScope.of(context).requestFocus(FocusNode());
            toastMessage.showToast(context, message);
            _shipmenttwoCubit.emitInitial();
          } else if (state is ShipmenttwoInitial) {
            //_shipmenttwoCubit.getCrossDockList(widget.arguments[1]);
            _shipmenttwoCubit.emitStart();
          }
        },
        // ignore: missing_return
        builder: (context, state) {
          if (state is ShipmenttwoStart) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 3,
                    child: buildBagIdText(),
                  ),
                  Expanded(
                    flex: 3,
                    child: buildStoreNameText(),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      _shipmenttwoCubit.productList.length.toString(),
                      style: TextStyle(fontSize: 22),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: buildReadProductField(delegate, _shipmenttwoCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 12,
                    child: buildProductList(_shipmenttwoCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: buildLocationIdField(delegate),
                  ),
                ],
              ),
            );
          } else if (state is ShipmenttwoInitial) {
            _shipmenttwoCubit.getCrossDockList(widget.arguments[1]);

            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is ShipmenttwoError) {
            return Center(
              child: Text(delegate.badRequestError),
            );
          } else if (state is ShipmenttwoClosed) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  ListView buildProductList(ShipmenttwoCubit _shipmenttwoCubit) {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(
        color: Colors.white60,
      ),
      itemCount: _shipmenttwoCubit.productList.length,
      itemBuilder: (context, index) {
        if (_shipmenttwoCubit.parameterResult == "1") {
          return Dismissible(
            onDismissed: (direction) {
              _shipmenttwoCubit.deleteProduct(
                username: widget.arguments[0][1],
                bagID: widget.arguments[1],
                barcode: _shipmenttwoCubit.productList[index],
                index: index,
              );
            },
            key: UniqueKey(),
            child: ListTile(
              leading: Icon(Icons.arrow_forward_ios_outlined),
              title: Text(_shipmenttwoCubit.productList[index]),
            ),
          );
        } else {
          return ListTile(
            leading: Icon(Icons.arrow_forward_ios_outlined),
            title: Text(_shipmenttwoCubit.productList[index]),
          );
        }
      },
    );
  }

  Form buildLocationIdField(S delegate) {
    return Form(
      key: _formKey2,
      child: TextFormField(
        controller: _textController2,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            return null;
          }
        },
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            this.locationID = value;
          }
        },
        decoration: InputDecoration(
          labelText: delegate.locationID,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  Form buildReadProductField(S delegate, ShipmenttwoCubit _shipmenttwoCubit) {
    return Form(
      key: _formKey,
      child: TextFormField(
        autofocus: true,
        focusNode: _focusNode,
        controller: _textController,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return (delegate.barcodeError);
          } else {
            return null;
          }
        },
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');

          if (value == null || value.isEmpty) {
            return (delegate.barcodeError);
          } else {
            _focusNode.requestFocus();
            _shipmenttwoCubit
                .readProduct(
                  username: widget.arguments[0][1],
                  bagID: widget.arguments[1],
                  barcode: _textController.text,
                )
                .then(
                  (value) => {
                    if (_shipmenttwoCubit.isDummy == true)
                      {
                        FocusScope.of(context).requestFocus(FocusNode()),
                        toastMessage.showToast(context, 'Dummy'),
                      }
                    else
                      {
                        _textController.clear(),
                        _focusNode.requestFocus(),
                      }
                  },
                );
          }
        },
        decoration: InputDecoration(
          labelText: delegate.productBarcode,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  SizedBox buildBagIdText() {
    return SizedBox(
      child: Text(
        widget.arguments[1],
        style: TextStyle(fontSize: 20),
      ),
    );
  }

  SizedBox buildStoreNameText() {
    return SizedBox(
      child: Text(
        widget.arguments[2],
        style: TextStyle(fontSize: 20),
      ),
    );
  }
}
