import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:deneme/views/Shipment/model/active_list_response.dart';
import 'package:deneme/views/Shipment2/model/delete_response_model.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';
import '../../Shipment/model/close_bag_response.dart';
import '../model/read_product_response.dart';

part 'shipmenttwo_state.dart';

class ShipmenttwoCubit extends Cubit<ShipmenttwoState> {
  ShipmenttwoCubit() : super(ShipmenttwoInitial());

  String message = "";
  List productList = [];
  double opacity = 0.0;
  bool isDummy;
  bool isBagClose = false;
  String parameterResult;

  void addItem(String item) {
    productList.insert(0, item);
    emit(ShipmenttwoInitial());
  }

  void deleteItem(int index) {
    productList.removeAt(index);
    emit(ShipmenttwoStart());
  }

  void emitInitial() {
    emit(ShipmenttwoInitial());
  }

  void emitStart() {
    emit(ShipmenttwoStart());
  }

  // ignore: missing_return
  Future<ReadProductResponse> readProduct({String username, String bagID, String barcode}) async {
    final response = await http.post(Uri.http(UriConst.uri, 'api/shipment/read_product'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, String>{"username": username, "Bag_ID": bagID, "barcode": barcode},
        ));
    if (response.statusCode == HttpStatus.ok) {
      isDummy = false;

      var responseBody = ReadProductResponse.fromJson(jsonDecode(response.body));
      parameterResult = responseBody.parameterResult;
      addItem(barcode);
    } else {
      isDummy = true;
    }
  }

  // ignore: missing_return
  Future<CloseBagResponse> closeBag({String username, String bagID, String locationID}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/shipment/close_bag'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username, "Bag_ID": bagID, "Location_ID": locationID},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = CloseBagResponse.fromJson(jsonDecode(response.body));
      emit(ShipmenttwoClosed(responseBody));
    } else if (response.statusCode == HttpStatus.unauthorized) {
      message = "incompatible";
      emit(ShipmenttwoError(message));
    } else if (response.statusCode == HttpStatus.forbidden) {
      message = "closedBag";
      emit(ShipmenttwoError(message));
    } else if (response.statusCode == HttpStatus.methodNotAllowed) {
      message = "notActive";
      emit(ShipmenttwoError(message));
    } else if (response.statusCode == HttpStatus.notFound) {
      message = "noLocation";
      emit(ShipmenttwoError(message));
    } else {
      message = "badRequestError";
      emit(ShipmenttwoError(message));
    }
  }

  Future<List<GetActiveListResponse>> getCrossDockList(String bagID) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/shipment/get_activelist'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"Bag_ID": bagID},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      emit(ShipmenttwoStart());
      var jsonBody = (jsonDecode(response.body)) as List;
      var crossDockList = jsonBody.map((e) => GetActiveListResponse.fromJson(e)).toList();
      for (var i = 0; i < crossDockList.length; i++) {
        this.productList.insert(i, crossDockList[i].barcode);
      }
      parameterResult = crossDockList[0].parameterResult;
    } else {}
  }

  Future<DeleteResponse> deleteProduct({String username, String bagID, String barcode, int index}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/shipment/delete_product'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          "username": username,
          "Bag_ID": bagID,
          "barcode": barcode,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      deleteItem(index);
    } else {
      return null;
    }
  }
}
