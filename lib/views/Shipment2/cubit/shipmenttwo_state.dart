part of 'shipmenttwo_cubit.dart';

@immutable
abstract class ShipmenttwoState {}

class ShipmenttwoInitial extends ShipmenttwoState {}

class ShipmenttwoError extends ShipmenttwoState {
  final String message;

  ShipmenttwoError(this.message);
}

class ShipmenttwoClosed extends ShipmenttwoState {
  final CloseBagResponse closeBagResponse;

  ShipmenttwoClosed(this.closeBagResponse);
}

class ShipmenttwoStart extends ShipmenttwoState {}
