// To parse this JSON data, do
//
//     final readProductResponse = readProductResponseFromJson(jsonString);

import 'dart:convert';

ReadProductResponse readProductResponseFromJson(String str) => ReadProductResponse.fromJson(json.decode(str));

String readProductResponseToJson(ReadProductResponse data) => json.encode(data.toJson());

class ReadProductResponse {
  ReadProductResponse({
    this.dummy,
    this.message,
    this.parameterResult,
  });

  String dummy;
  String message;
  String parameterResult;

  factory ReadProductResponse.fromJson(Map<String, dynamic> json) => ReadProductResponse(
        dummy: json["dummy"],
        message: json["message"],
        parameterResult: json["parameter_result"],
      );

  Map<String, dynamic> toJson() => {
        "dummy": dummy,
        "message": message,
        "parameter_result": parameterResult,
      };
}
