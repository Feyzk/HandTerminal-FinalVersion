// To parse this JSON data, do
//
//     final deleteResponse = deleteResponseFromJson(jsonString);

import 'dart:convert';

DeleteResponse deleteResponseFromJson(String str) => DeleteResponse.fromJson(json.decode(str));

String deleteResponseToJson(DeleteResponse data) => json.encode(data.toJson());

class DeleteResponse {
  DeleteResponse({
    this.status,
  });

  bool status;

  factory DeleteResponse.fromJson(Map<String, dynamic> json) => DeleteResponse(
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
      };
}
