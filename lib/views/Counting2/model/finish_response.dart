import 'dart:convert';

FinishResponse finishResponseFromJson(String str) =>
    FinishResponse.fromJson(json.decode(str));

String finishResponseToJson(FinishResponse data) => json.encode(data.toJson());

class FinishResponse {
  FinishResponse({
    this.status,
  });

  bool status;

  factory FinishResponse.fromJson(Map<String, dynamic> json) => FinishResponse(
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
      };
}
