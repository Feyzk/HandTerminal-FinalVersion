import 'dart:convert';

List<LocationListResponse> locationListResponseFromJson(String str) =>
    List<LocationListResponse>.from(
        json.decode(str).map((x) => LocationListResponse.fromJson(x)));

String locationListResponseToJson(List<LocationListResponse> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LocationListResponse {
  LocationListResponse({
    this.storeName,
  });

  String storeName;

  factory LocationListResponse.fromJson(Map<String, dynamic> json) =>
      LocationListResponse(
        storeName: json["Store_Name"],
      );

  Map<String, dynamic> toJson() => {
        "Store_Name": storeName,
      };
}
