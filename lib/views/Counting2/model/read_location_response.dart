import 'dart:convert';

ReadLocationResponse readLocationResponseFromJson(String str) => ReadLocationResponse.fromJson(json.decode(str));

String readLocationResponseToJson(ReadLocationResponse data) => json.encode(data.toJson());

class ReadLocationResponse {
  ReadLocationResponse({
    this.status,
    this.assignmentId,
    this.parameterResult,
  });

  bool status;
  String assignmentId;
  String parameterResult;

  factory ReadLocationResponse.fromJson(Map<String, dynamic> json) => ReadLocationResponse(
        status: json["status"],
        assignmentId: json["Assignment_ID"],
        parameterResult: json["parameter_result"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "Assignment_ID": assignmentId,
        "parameter_result": parameterResult,
      };
}
