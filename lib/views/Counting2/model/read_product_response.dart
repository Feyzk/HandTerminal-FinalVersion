import 'dart:convert';

ReadProductResponse readProductResponseFromJson(String str) =>
    ReadProductResponse.fromJson(json.decode(str));

String readProductResponseToJson(ReadProductResponse data) =>
    json.encode(data.toJson());

class ReadProductResponse {
  ReadProductResponse({
    this.read,
    this.message,
  });

  bool read;
  String message;

  factory ReadProductResponse.fromJson(Map<String, dynamic> json) =>
      ReadProductResponse(
        read: json["read"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "read": read,
        "message": message,
      };
}
