import 'package:deneme/base/consts/route_consts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/toast_message.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/countingtwo_cubit.dart';

class CountingTwoView extends StatefulWidget {
  final List arguments;

  const CountingTwoView({Key key, this.arguments}) : super(key: key);
  @override
  _CountingTwoViewState createState() => _CountingTwoViewState();
}

class _CountingTwoViewState extends State<CountingTwoView> {
  TextEditingController _textController;
  TextEditingController _textController2;
  FocusNode _focusNode;
  ToastMessage toastMessage = ToastMessage();
  @override
  void initState() {
    _textController = TextEditingController();
    _textController2 = TextEditingController();
    _focusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _textController.dispose();
    _textController2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _countingTwoCubit = BlocProvider.of<CountingtwoCubit>(context);
    final S delegate = S.of(context);

    _countingTwoCubit.getLocationList(username: widget.arguments[0][1], countingID: widget.arguments[1]);
    return BaseView(
      username: widget.arguments[0][1],
      appBarTitle: delegate.counting,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios_outlined,
        ),
        onPressed: () {
          Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0]);
        },
      ),
      home: BlocBuilder<CountingtwoCubit, CountingtwoState>(
        builder: (context, state) {
          if (state is CountingtwoInitial) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is CountingtwoList) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
              child: Column(
                children: [
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: buildLocationBarcodeField(delegate, _countingTwoCubit, context),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Expanded(
                    flex: 15,
                    child: buildListView(state),
                  )
                ],
              ),
            );
          } else {
            return Center(
              child: Text((state as CountingtwoError).message),
            );
          }
        },
      ),
    );
  }

  ListView buildListView(CountingtwoList state) {
    return ListView.separated(
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(state.locationListResponse[index].storeName ?? ""),
          );
        },
        separatorBuilder: (context, index) => Divider(color: Colors.white60),
        itemCount: state.locationListResponse.length);
  }

  TextFormField buildLocationBarcodeField(S delegate, CountingtwoCubit _countingTwoCubit, BuildContext context) {
    return TextFormField(
      autofocus: true,
      controller: _textController,
      onFieldSubmitted: (value) {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        if (value == null || value.isEmpty) {
          return delegate.barcodeError;
        } else {
          _countingTwoCubit.readLocation(username: widget.arguments[0][1], countingID: widget.arguments[1], locationID: _textController.text).then(
                (response) => {
                  if (_countingTwoCubit.locationStatus == true)
                    {
                      _textController.clear(),
                      SystemChannels.textInput.invokeMethod('TextInput.hide'),
                      Navigator.of(context).pushNamed(COUNTING3, arguments: [
                        widget.arguments,
                        _countingTwoCubit.assignmentId,
                        widget.arguments[1],
                        _countingTwoCubit.parameterResult,
                      ])
                    }
                  else
                    {
                      FocusScope.of(context).requestFocus(FocusNode()),
                      toastMessage.showToast(context, delegate.locationBarcodeError),
                    }
                },
              );
        }
      },
      decoration: InputDecoration(
        labelText: delegate.locationBarcode,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
      ),
    );
  }

  // TextFormField buildProductBarcodeField(S delegate, CountingtwoCubit _countingTwoCubit, BuildContext context) {
  //   return TextFormField(
  //     focusNode: _focusNode,
  //     controller: _textController2,
  //     onFieldSubmitted: (value) {
  //       SystemChannels.textInput.invokeMethod('TextInput.hide');
  //       if (value == null || value.isEmpty) {
  //         return delegate.barcodeError;
  //       } else {
  //         _countingTwoCubit.readProduct(username: widget.arguments[0][1], countingID: widget.arguments[1], barcode: _textController2.text).then(
  //               (response) => {
  //                 if (response.read == true)
  //                   {
  //                     FocusScope.of(context).requestFocus(FocusNode()),
  //                     toastMessage.showToast(context, delegate.success),
  //                     Future.delayed(const Duration(seconds: 2), () {}),
  //                     _textController.clear(),
  //                     _textController2.clear(),
  //                   }
  //                 else
  //                   {FocusScope.of(context).requestFocus(FocusNode()), toastMessage.showToast(context, delegate.notUserandCount)}
  //               },
  //             );
  //       }
  //     },
  //     decoration: InputDecoration(
  //       labelText: delegate.productBarcode,
  //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
  //     ),
  //   );
  // }
}
