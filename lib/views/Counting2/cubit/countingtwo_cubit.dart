import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';
import '../model/finish_response.dart';
import '../model/location_list_response.dart';
import '../model/read_location_response.dart';
import '../model/read_product_response.dart';

part 'countingtwo_state.dart';

class CountingtwoCubit extends Cubit<CountingtwoState> {
  CountingtwoCubit() : super(CountingtwoInitial());

  bool locationStatus;
  List locationList = [];
  String assignmentId;
  String parameterResult;

  void emitInitial() {
    emit(CountingtwoInitial());
  }

  Future<ReadLocationResponse> readLocation({
    String username,
    String countingID,
    String locationID,
  }) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/counting/read_location'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{'username': username, "Counting_ID": countingID, "Location_ID": locationID},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = ReadLocationResponse.fromJson(jsonDecode(response.body));
      assignmentId = responseBody.assignmentId;
      parameterResult = responseBody.parameterResult;
      this.locationStatus = true;
    } else {
      this.locationStatus = false;
    }
  }

  // Future<ReadProductResponse> readProduct({String username, String countingID, String barcode}) async {
  //   final response = await http.post(
  //     Uri.http(UriConst.uri, 'api/counting/read_barcode'),
  //     headers: <String, String>{
  //       'Content-Type': 'application/json; charset=UTF-8',
  //     },
  //     body: jsonEncode(
  //       <String, String>{'username': username, "Counting_ID": countingID, "barcode": barcode},
  //     ),
  //   );
  //   if (response.statusCode == HttpStatus.ok) {
  //     var responseBody = ReadProductResponse.fromJson(jsonDecode(response.body));
  //     return responseBody;
  //   } else
  //     emit(CountingtwoError('badRequest'));
  // }

  Future<List<LocationListResponse>> getLocationList({String username, String countingID}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/counting/list_locations'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{'username': username, "Counting_ID": countingID},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = (jsonDecode(response.body)) as List;
      var jsonList = jsonBody.map((e) => LocationListResponse.fromJson(e)).toList();
      this.locationList = jsonList;

      emit(CountingtwoList(jsonList));
    } else {
      emit(CountingtwoError('badRequest'));
    }
  }

  Future<FinishResponse> finish({String username, String countingID}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/counting/finish'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          'username': username,
          "Counting_ID": countingID,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = FinishResponse.fromJson(
        jsonDecode(response.body),
      );
      return responseBody;
    } else {
      return null;
    }
  }
}
