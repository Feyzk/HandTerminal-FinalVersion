part of 'countingtwo_cubit.dart';

@immutable
abstract class CountingtwoState {}

class CountingtwoInitial extends CountingtwoState {}

class CountingtwoError extends CountingtwoState {
  final String message;

  CountingtwoError(this.message);
}

class CountingtwoList extends CountingtwoState {
  final List<LocationListResponse> locationListResponse;

  CountingtwoList(this.locationListResponse);
}
