import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../base/consts/route_consts.dart';
import '../../generated/l10n.dart';
import 'language_model.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    WidgetsFlutterBinding.ensureInitialized();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Spacer(
              flex: 5,
            ),
            Expanded(
              flex: 10,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  buildEnglishInkWell(context),
                  buildTurkishInkWell(context),
                ],
              ),
            ),
            Expanded(
              flex: 10,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  buildRussianInkWell(context),
                  buildUzbekistanInkwell(context),
                ],
              ),
            ),
            Spacer(flex: 5),
          ],
        ),
      ),
    );
  }

  InkWell buildUzbekistanInkwell(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, LOGIN_ROUTE);
        setState(
          () {
            S.load(Locale("uz", "UZ"));
          },
        );
      },
      child: Column(
        children: [
          Text(
            Language.languageList()[3].name,
            style: TextStyle(fontSize: 96),
          ),
          Text(
            Language.languageList()[3].flag,
            style: TextStyle(fontSize: 32),
          ),
        ],
      ),
    );
  }

  InkWell buildRussianInkWell(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, LOGIN_ROUTE);
        setState(
          () {
            S.load(Locale("ru", "RU"));
          },
        );
      },
      child: Column(
        children: [
          Text(
            Language.languageList()[2].name,
            style: TextStyle(fontSize: 96),
          ),
          Text(
            Language.languageList()[2].flag,
            style: TextStyle(fontSize: 32),
          ),
        ],
      ),
    );
  }

  InkWell buildTurkishInkWell(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, LOGIN_ROUTE);
        setState(
          () {
            S.load(Locale("tr", "TR"));
          },
        );
      },
      child: Column(
        children: [
          Text(
            Language.languageList()[1].name,
            style: TextStyle(fontSize: 96),
          ),
          Text(
            Language.languageList()[1].flag,
            style: TextStyle(fontSize: 32),
          ),
        ],
      ),
    );
  }

  InkWell buildEnglishInkWell(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(
          () {
            Navigator.pushNamed(context, LOGIN_ROUTE);
            S.load(Locale("en", "US"));
          },
        );
      },
      child: Column(
        children: [
          Text(
            Language.languageList()[0].name,
            style: TextStyle(fontSize: 96),
          ),
          Text(
            Language.languageList()[0].flag,
            style: TextStyle(fontSize: 32),
          ),
        ],
      ),
    );
  }
}
