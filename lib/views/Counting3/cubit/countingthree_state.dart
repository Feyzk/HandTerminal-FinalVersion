part of 'countingthree_cubit.dart';

@immutable
abstract class CountingthreeState {}

class CountingthreeInitial extends CountingthreeState {}

class CountingthreeError extends CountingthreeState {
  final String message;

  CountingthreeError(this.message);
}

class CountingthreeList extends CountingthreeState {}
