import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:deneme/base/consts/uri_consts.dart';
import 'package:deneme/views/Counting3/model/get_product_response.dart';
import 'package:deneme/views/Counting3/model/read_product_response.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

part 'countingthree_state.dart';

class CountingthreeCubit extends Cubit<CountingthreeState> {
  CountingthreeCubit() : super(CountingthreeInitial());

  String parameterResult;
  List countingList = [];

  void deleteProductFromList(index) {
    countingList.removeAt(index);
    emit(CountingthreeList());
  }

  void addItem(item) {
    countingList.insert(0, item);
    emit(CountingthreeList());
  }

  Future<List<GetProductResponse>> getProducts({String username, String assignmentId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/counting/get_products'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          'username': username,
          "Assignment_ID": assignmentId,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = (jsonDecode(response.body)) as List;
      emit(CountingthreeList());
      var jsonList = jsonBody.map((e) => GetProductResponse.fromJson(e)).toList();
      for (var i = 0; i < jsonList.length; i++) {
        countingList.insert(0, jsonList[i].barcode);
      }
    } else {
      emit(CountingthreeError('message'));
    }
  }

  Future<CountingProductResponse> readProduct({String username, String assignmentId, String barcode}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/counting/read_barcode'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{'username': username, "Assignment_ID": assignmentId, "barcode": barcode},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = CountingProductResponse.fromJson(jsonDecode(response.body));

      return responseBody;
    } else {}
  }

  Future<bool> deleteProduct({String username, String assignmentId, String barcode}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/counting/delete_product'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{'username': username, "Assignment_ID": assignmentId, "barcode": barcode},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> finishCounting({String username, String assignmentId, String countingId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/counting/finish'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          'username': username,
          "Assignment_ID": assignmentId,
          "Counting_ID": countingId,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      return true;
    } else {
      return false;
    }
  }
}
