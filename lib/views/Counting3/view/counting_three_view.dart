import 'package:deneme/base/base-view/base_view.dart';
import 'package:deneme/base/consts/route_consts.dart';
import 'package:deneme/base/consts/toast_message.dart';
import 'package:deneme/generated/l10n.dart';
import 'package:deneme/views/Counting3/cubit/countingthree_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../../../base/extension/content_extension.dart';

class CountingThreeView extends StatefulWidget {
  final List arguments;
  const CountingThreeView({Key key, this.arguments}) : super(key: key);

  @override
  _CountingThreeViewState createState() => _CountingThreeViewState();
}

class _CountingThreeViewState extends State<CountingThreeView> {
  TextEditingController _textEditingController;
  FocusNode _focusNode;
  ToastMessage _toastMessage;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
    _focusNode = FocusNode();
    _toastMessage = ToastMessage();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _countingThreeCubit = BlocProvider.of<CountingthreeCubit>(context);
    final S delegate = S.of(context);
    _countingThreeCubit.getProducts(username: widget.arguments[0][0][1], assignmentId: widget.arguments[1]);

    return BaseView(
      username: widget.arguments[0][1],
      appBarTitle: delegate.counting,
      finishLeading: TextButton(
        child: Text(delegate.finish),
        onPressed: () {
          _countingThreeCubit
              .finishCounting(
                username: widget.arguments[0][0][1],
                assignmentId: widget.arguments[1],
                countingId: widget.arguments[2],
              )
              .then(
                (response) => {
                  if (response == true)
                    {
                      Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0][0]),
                    }
                },
              );
        },
      ),
      home: BlocConsumer<CountingthreeCubit, CountingthreeState>(
        listener: (context, state) {
          // TODO: implement listener
        },
        builder: (context, state) {
          if (state is CountingthreeInitial) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is CountingthreeList) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
              child: Column(
                children: [
                  Expanded(
                    flex: 3,
                    child: Text(
                      _countingThreeCubit.countingList.length.toString(),
                      style: TextStyle(fontSize: 24),
                    ),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: buildProductBarcodeField(delegate, _countingThreeCubit, context),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Expanded(
                    flex: 15,
                    child: buildListView(state, _countingThreeCubit),
                  )
                ],
              ),
            );
          } else {
            return Center(
              child: Text((state as CountingthreeError).message),
            );
          }
        },
      ),
    );
  }

  TextFormField buildProductBarcodeField(S delegate, CountingthreeCubit _countingThreeCubit, BuildContext context) {
    return TextFormField(
      focusNode: _focusNode,
      controller: _textEditingController,
      autofocus: true,
      onFieldSubmitted: (value) {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        if (value == null || value.isEmpty) {
          return delegate.barcodeError;
        } else {
          _countingThreeCubit
              .readProduct(username: widget.arguments[0][1], assignmentId: widget.arguments[1], barcode: _textEditingController.text)
              .then(
                (response) => {
                  if (response.read == true)
                    {
                      SystemChannels.textInput.invokeMethod('TextInput.hide'),
                      FocusScope.of(context).requestFocus(_focusNode),
                      _countingThreeCubit.addItem(_textEditingController.text),
                      _textEditingController.clear(),
                    }
                  else
                    {
                      FocusScope.of(context).requestFocus(FocusNode()),
                      _toastMessage.showToast(
                        context,
                        delegate.notUserandCount,
                      ),
                    }
                },
              );
        }
      },
      decoration: InputDecoration(
        labelText: delegate.productBarcode,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
      ),
    );
  }

  ListView buildListView(CountingthreeList state, CountingthreeCubit _countingThreeCubit) {
    return ListView.separated(
        itemBuilder: (context, index) {
          if (widget.arguments[3] == "1") {
            return Dismissible(
              onDismissed: (direction) {
                _countingThreeCubit
                    .deleteProduct(
                      username: widget.arguments[0][0][1],
                      assignmentId: widget.arguments[1],
                      barcode: _countingThreeCubit.countingList[index],
                    )
                    .then(
                      (value) => _countingThreeCubit.deleteProductFromList(index),
                    );
              },
              key: UniqueKey(),
              child: ListTile(
                title: Text(
                  _countingThreeCubit.countingList[index],
                ),
              ),
            );
          } else {
            return ListTile(title: Text(_countingThreeCubit.countingList[index]));
          }
        },
        separatorBuilder: (context, index) => Divider(color: Colors.white60),
        itemCount: _countingThreeCubit.countingList.length);
  }
}
