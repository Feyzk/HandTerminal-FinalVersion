// To parse this JSON data, do
//
//     final getProductResponse = getProductResponseFromJson(jsonString);

import 'dart:convert';

List<GetProductResponse> getProductResponseFromJson(String str) =>
    List<GetProductResponse>.from(json.decode(str).map((x) => GetProductResponse.fromJson(x)));

String getProductResponseToJson(List<GetProductResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetProductResponse {
  GetProductResponse({
    this.barcode,
    this.parameterResult,
  });

  String barcode;
  String parameterResult;

  factory GetProductResponse.fromJson(Map<String, dynamic> json) => GetProductResponse(
        barcode: json["Barcode"],
        parameterResult: json["parameter_result"],
      );

  Map<String, dynamic> toJson() => {
        "Barcode": barcode,
        "parameter_result": parameterResult,
      };
}
