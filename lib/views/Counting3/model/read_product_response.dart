import 'dart:convert';

CountingProductResponse CountingProductResponseFromJson(String str) => CountingProductResponse.fromJson(json.decode(str));

String CountingProductResponseToJson(CountingProductResponse data) => json.encode(data.toJson());

class CountingProductResponse {
  CountingProductResponse({
    this.read,
    this.message,
  });

  bool read;
  String message;

  factory CountingProductResponse.fromJson(Map<String, dynamic> json) => CountingProductResponse(
        read: json["read"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "read": read,
        "message": message,
      };
}
