part of 'carriagetwo_cubit.dart';

@immutable
abstract class CarriagetwoState {}

class CarriagetwoInitial extends CarriagetwoState {}

class CarriagetwoListStart extends CarriagetwoState {
  final List<CarriageActiveOrderListResponse> activeOrderListResponse;

  CarriagetwoListStart(this.activeOrderListResponse);
}

class CarriagetwoError extends CarriagetwoState {
  final String message;

  CarriagetwoError(this.message);
}
