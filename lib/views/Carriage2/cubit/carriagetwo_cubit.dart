import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';
import '../model/active_bag_control_response.dart';
import '../model/active_location_control_response.dart';
import '../model/carriage_active_order_list_response.dart';

part 'carriagetwo_state.dart';

class CarriagetwoCubit extends Cubit<CarriagetwoState> {
  CarriagetwoCubit() : super(CarriagetwoInitial());

  List orderList;
  List errors = ["badRequestError"];
  bool locationStatus;
  bool bagStatus;

  void removeOrder() {
    orderList.removeAt(0);
  }

  // ignore: missing_return
  Future<CarriageActiveOrderListResponse> getActiveOrderList({String username, String carriageOrderId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/carriage/active_order_list'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username, "Carriage_Order_ID": carriageOrderId},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = ((jsonDecode(response.body))) as List;

      var activeOrderList = jsonBody.map((e) => CarriageActiveOrderListResponse.fromJson(e)).toList();

      this.orderList = activeOrderList;

      emit(CarriagetwoListStart(activeOrderList));
    } else {
      emit(CarriagetwoError(errors[0]));
    }
  }

  // ignore: missing_return
  Future<ActiveLocationControlResponse> checkLocationControl({String username, String carriageOrderId, String locationId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/carriage/active_location_control'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          "username": username,
          "Carriage_Order_ID": carriageOrderId,
          "Location_ID": locationId,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      locationStatus = true;
      var jsonBody = ActiveLocationControlResponse.fromJson(
        jsonDecode(response.body),
      );
      return jsonBody;
    } else {
      locationStatus = false;
    }
  }

  // ignore: missing_return
  Future<ActiveBagControlResponse> checkBagControl({String username, String carriageOrderId, String locationId, String bagId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/carriage/active_bag_control'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username, "Carriage_Order_ID": carriageOrderId, "Location_ID": locationId, "Bag_ID": bagId},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      bagStatus = true;
      var jsonBody = ActiveBagControlResponse.fromJson(
        jsonDecode(response.body),
      );
      return jsonBody;
    } else {
      bagStatus = false;
    }
  }

  Future<bool> finishOrder({String username, String carriageOrderId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/carriage/order_finish'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username, "Carriage_Order_ID": carriageOrderId},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> dismissOrder({String username, String carriageOrderId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/carriage/order_dismiss'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username, "Carriage_Order_ID": carriageOrderId},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      return true;
    } else {
      return false;
    }
  }
}
