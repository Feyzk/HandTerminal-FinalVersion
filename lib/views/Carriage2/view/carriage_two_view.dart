import 'package:deneme/base/consts/route_consts.dart';
import 'package:deneme/base/consts/toast_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/carriagetwo_cubit.dart';

class CarriageTwoView extends StatefulWidget {
  final List arguments;

  const CarriageTwoView({Key key, this.arguments}) : super(key: key);
  @override
  _CarriageTwoViewState createState() => _CarriageTwoViewState();
}

class _CarriageTwoViewState extends State<CarriageTwoView> {
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  TextEditingController _textController;
  TextEditingController _textController2;
  FocusNode focusNode;
  FocusNode focusNode2;
  ToastMessage toastMessage = ToastMessage();

  @override
  void initState() {
    _textController = TextEditingController();
    _textController2 = TextEditingController();
    focusNode = FocusNode();
    focusNode2 = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    _textController2.dispose();
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final _carriageTwoCubit = BlocProvider.of<CarriagetwoCubit>(context);
    _carriageTwoCubit.getActiveOrderList(username: widget.arguments[0][1], carriageOrderId: widget.arguments[1]);
    return BaseView(
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios_outlined,
        ),
        onPressed: () {
          _carriageTwoCubit.dismissOrder(username: widget.arguments[0][1], carriageOrderId: widget.arguments[1]).then((value) {
            Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0]);
          });
        },
      ),
      finishLeading: TextButton(
        child: Text('Finish'),
        onPressed: () {
          _carriageTwoCubit.finishOrder(username: widget.arguments[0][1], carriageOrderId: widget.arguments[1]).then(
                (_) => {Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0])},
              );
        },
      ),
      username: widget.arguments[0][1],
      appBarTitle: delegate.carriage,
      home: BlocConsumer<CarriagetwoCubit, CarriagetwoState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is CarriagetwoInitial) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is CarriagetwoListStart) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 2,
                    child: Text(
                      widget.arguments[2],
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: buildLocationBarcodeField(delegate, _carriageTwoCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: buildBagBarcodeField(delegate, _carriageTwoCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 15,
                    child: buildProductList(state),
                  ),
                ],
              ),
            );
          } else if (state is CarriagetwoError) {
            return Center(child: Text(delegate.badRequestError));
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  ListView buildProductList(CarriagetwoListStart state) {
    return ListView.separated(
        itemBuilder: (context, index) {
          return Card(
            child: ListTile(
              title: Text(state.activeOrderListResponse[index].locationId),
              subtitle: Text(state.activeOrderListResponse[index].shipmentBagId),
            ),
          );
        },
        separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
        itemCount: state.activeOrderListResponse.length);
  }

  SizedBox buildFinishButton(CarriagetwoListStart state, CarriagetwoCubit _carriageTwoCubit, BuildContext context) {
    return SizedBox(
      width: context.dynamicWidth(0.8),
      height: context.dynamicHeigt(0.1),
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.black87),
        ),
        onPressed: state.activeOrderListResponse.length == 0
            ? () => _carriageTwoCubit
                .finishOrder(username: widget.arguments[0][1], carriageOrderId: widget.arguments[1])
                .then((_) => {Navigator.pop(context)})
            : null,
        child: Text(
          'Finish',
        ),
      ),
    );
  }

  Form buildBagBarcodeField(S delegate, CarriagetwoCubit _carriageTwoCubit) {
    return Form(
      key: _formKey2,
      child: TextFormField(
        focusNode: focusNode,
        controller: _textController2,
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            _carriageTwoCubit
                .checkBagControl(
                    username: widget.arguments[0][1],
                    carriageOrderId: widget.arguments[1],
                    locationId: _textController.text,
                    bagId: _textController2.text)
                .then((value) => {
                      if (_carriageTwoCubit.bagStatus == false)
                        {
                          FocusScope.of(context).requestFocus(FocusNode()),
                          toastMessage.showToast(context, delegate.wrongBag),
                        }
                      else
                        {
                          _carriageTwoCubit.getActiveOrderList(username: widget.arguments[0][1], carriageOrderId: widget.arguments[1]),
                          Future.delayed(const Duration(seconds: 2), () {}),
                          _textController.clear(),
                          _textController2.clear(),
                          focusNode2.requestFocus()
                        }
                    });
          }
        },
        decoration: InputDecoration(
          labelText: 'BagBarcode',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  Form buildLocationBarcodeField(S delegate, CarriagetwoCubit _carriageTwoCubit) {
    return Form(
      key: _formKey,
      child: TextFormField(
        focusNode: focusNode2,
        autofocus: true,
        controller: _textController,
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            _carriageTwoCubit
                .checkLocationControl(username: widget.arguments[0][1], carriageOrderId: widget.arguments[1], locationId: _textController.text)
                .then(
                  (value) => {
                    if (_carriageTwoCubit.locationStatus == false)
                      {
                        FocusScope.of(context).requestFocus(FocusNode()),
                        toastMessage.showToast(context, delegate.locationBarcodeError),
                      }
                    else
                      {
                        focusNode.requestFocus(),
                      }
                  },
                );
          }
        },
        decoration: InputDecoration(
          labelText: 'LocationBarcode',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
