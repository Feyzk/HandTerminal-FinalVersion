import 'dart:convert';

ActiveBagControlResponse activeBagControlResponseFromJson(String str) =>
    ActiveBagControlResponse.fromJson(json.decode(str));

String activeBagControlResponseToJson(ActiveBagControlResponse data) =>
    json.encode(data.toJson());

class ActiveBagControlResponse {
  ActiveBagControlResponse({
    this.bag,
    this.message,
  });

  bool bag;
  String message;

  factory ActiveBagControlResponse.fromJson(Map<String, dynamic> json) =>
      ActiveBagControlResponse(
        bag: json["bag"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "bag": bag,
        "message": message,
      };
}
