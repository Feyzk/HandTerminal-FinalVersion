import 'dart:convert';

List<CarriageActiveOrderListResponse> carriageActiveOrderListResponseFromJson(
        String str) =>
    List<CarriageActiveOrderListResponse>.from(json
        .decode(str)
        .map((x) => CarriageActiveOrderListResponse.fromJson(x)));

String carriageActiveOrderListResponseToJson(
        List<CarriageActiveOrderListResponse> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CarriageActiveOrderListResponse {
  CarriageActiveOrderListResponse({
    this.shipmentBagId,
    this.locationId,
  });

  String shipmentBagId;
  String locationId;

  factory CarriageActiveOrderListResponse.fromJson(Map<String, dynamic> json) =>
      CarriageActiveOrderListResponse(
        shipmentBagId: json["Shipment_Bag_ID"],
        locationId: json["Location_ID"],
      );

  Map<String, dynamic> toJson() => {
        "Shipment_Bag_ID": shipmentBagId,
        "Location_ID": locationId,
      };
}
