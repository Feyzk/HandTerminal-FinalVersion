import 'dart:convert';

ActiveLocationControlResponse activeLocationControlResponseFromJson(
        String str) =>
    ActiveLocationControlResponse.fromJson(json.decode(str));

String activeLocationControlResponseToJson(
        ActiveLocationControlResponse data) =>
    json.encode(data.toJson());

class ActiveLocationControlResponse {
  ActiveLocationControlResponse({
    this.location,
    this.message,
  });

  bool location;
  String message;

  factory ActiveLocationControlResponse.fromJson(Map<String, dynamic> json) =>
      ActiveLocationControlResponse(
        location: json["location"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "location": location,
        "message": message,
      };
}
