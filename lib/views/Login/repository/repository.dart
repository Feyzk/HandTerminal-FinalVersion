import '../model/login_response_model.dart';
import 'network_service.dart';

class Repository {
  final NetworkService networkService;

  Repository({this.networkService});

  Future<LoginResponseModel> login({String username, String password}) async {
    final loginResponse =
        await networkService.login(username: username, password: password);
    return LoginResponseModel.fromJson(loginResponse);
  }
}
