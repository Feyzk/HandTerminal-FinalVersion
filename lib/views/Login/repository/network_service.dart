import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';

class NetworkService {
  final baseUrl = "https://reqres.in/api/users";

  // ignore: missing_return
  Future<Map> login({String username, String password}) async {
    try {
      final data = jsonEncode(<String, String>{"username": username, "password": password});
      final response = await post(Uri.parse(baseUrl), body: data);
      switch (response.statusCode) {
        case HttpStatus.ok:
          return jsonDecode(response.body);
          break;
        default:
      }
    } catch (e) {}
  }
}
