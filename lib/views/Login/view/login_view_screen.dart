import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../base/consts/route_consts.dart';
import '../../../base/consts/toast_message.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/login_cubit.dart';

class LoginViewScreen extends StatefulWidget {
  @override
  _LoginViewScreenState createState() => _LoginViewScreenState();
}

class _LoginViewScreenState extends State<LoginViewScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _usernameController;
  TextEditingController _passwordController;
  FocusNode _focusNode;
  ToastMessage toastMessage = ToastMessage();

  @override
  void initState() {
    _usernameController = TextEditingController();
    _passwordController = TextEditingController();
    _focusNode = FocusNode();

    super.initState();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    final delegate = S.of(context);
    final _loginCubit = BlocProvider.of<LoginCubit>(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(backgroundColor: Colors.transparent),
        body: BlocConsumer<LoginCubit, LoginState>(
          listener: (context, state) {
            if (state is LoginCompleted) {
              Navigator.pushNamedAndRemoveUntil(context, MENU_ROUTE, (route) => false, arguments: [_loginCubit.roles, _loginCubit.username]);
            } else if (state is LoginError) {
              String message = state.errorMessage;
              if (message == _loginCubit.errors[0]) {
                message = delegate.notFoundError;
              } else if (message == _loginCubit.errors[1]) {
                message = delegate.badRequestError;
              } else {
                message = delegate.alreadyOnlineError;
              }
              toastMessage.showToast(context, message);
            }
          },
          builder: (context, state) {
            if (!(state is LoginError)) {
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
                child: Column(
                  children: [
                    Expanded(
                      flex: 10,
                      child: Column(
                        children: [
                          Expanded(
                            child: buildUsernameField(delegate),
                          ),
                          Spacer(flex: 1),
                          Expanded(
                            child: buildPasswordField(delegate),
                          ),
                        ],
                      ),
                    ),
                    Spacer(
                      flex: 5,
                    ),
                    Expanded(
                      flex: 3,
                      child: buildLoginButton(_loginCubit, delegate, context),
                    ),
                    Spacer(
                      flex: 4,
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        children: [
                          Text(
                            'CK WMS',
                            style: TextStyle(fontSize: 24),
                          ),
                          Text(
                            'Version 1.0.0',
                            style: TextStyle(fontSize: 22),
                          ),
                        ],
                      ),
                    ),
                    Spacer(
                      flex: 4,
                    ),
                    Expanded(
                        flex: 3,
                        child: SizedBox(
                          width: context.dynamicWidth(0.8),
                          height: context.dynamicHeigt(0.05),
                          child: ElevatedButton(
                            style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.black87)),
                            onPressed: () async {
                              Navigator.pushNamed(context, CHANGE_IP);
                            },
                            child: Text(delegate.changeIp),
                          ),
                        )),
                  ],
                ),
              );
            } else if (state is LoginCompleted) {
              return Center(child: CircularProgressIndicator());
            } else {
              _loginCubit.emitInitial();
              return Center(
                child: Text(''),
              );
            }
          },
        ),
      ),
    );
  }

  SizedBox buildLoginButton(LoginCubit _loginCubit, S delegate, BuildContext context) {
    return SizedBox(
      width: context.dynamicWidth(0.8),
      height: context.dynamicHeigt(0.05),
      child: ElevatedButton(
        style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.black87)),
        onPressed: () async {
          final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
          sharedPreferences.setString('username', _usernameController.text);

          FocusScope.of(context).requestFocus(FocusNode());
          _loginCubit.login(username: _usernameController.text, password: _passwordController.text, version: '1.0.0');
        },
        child: Text(delegate.login),
      ),
    );
  }

  TextFormField buildPasswordField(S delegate) {
    return TextFormField(
      focusNode: _focusNode,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      controller: _passwordController,
      validator: (password) {
        if (password == null || password.isEmpty) {
          return (delegate.enterPassword);
        }
        return null;
      },
    );
  }

  TextFormField buildUsernameField(S delegate) {
    return TextFormField(
      autofocus: true,
      decoration: InputDecoration(
        hintText: 'Username',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      controller: _usernameController,
      validator: (username) {
        if (username == null || username.isEmpty) {
          return (delegate.enterUsername);
        }
        return null;
      },
      onFieldSubmitted: (username) {
        if (username == null || username.isEmpty) {
          return (delegate.enterUsername);
        } else {
          _focusNode.requestFocus();
        }
      },
    );
  }
}
