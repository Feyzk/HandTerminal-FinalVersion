import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../base/consts/uri_consts.dart';
import '../model/login_response_model.dart';
import '../repository/repository.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final Repository repository;
  LoginCubit({this.repository}) : super(LoginInitial());

  List roles;
  String username = '';

  List errors = ["notFoundError", "badRequestError", "alreadyOnlineError"];

  void emitInitial() {
    emit(LoginInitial());
  }

  // ignore: missing_return
  Future<LoginResponseModel> login({String username, String password, String version}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/auth/login'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{'username': username, 'password': password, "version": version}),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseModel = LoginResponseModel.fromJson(jsonDecode(response.body));

      final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString('username', responseModel.username);
      sharedPreferences.setStringList(
        'roles',
        responseModel.roles.map((e) => e.toString()).toList(),
      );

      this.username = sharedPreferences.getString('username');
      this.roles = sharedPreferences.getStringList('roles').map((e) => int.parse(e)).toList();

      emit(LoginCompleted(responseModel));
    } else if (response.statusCode == HttpStatus.notFound) {
      emit(LoginError(errors[0]));
    } else if (response.statusCode == HttpStatus.badRequest) {
      emit(LoginError(errors[1]));
    } else {
      emit(LoginError(errors[2]));
    }
  }
}
