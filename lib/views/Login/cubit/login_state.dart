part of 'login_cubit.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginCompleted extends LoginState {
  final LoginResponseModel loginResponseModel;

  LoginCompleted(this.loginResponseModel);
}

class LoginError extends LoginState {
  final String errorMessage;

  LoginError(this.errorMessage);
}
