import 'dart:convert';

LoginResponseModel loginResponseModelFromJson(String str) =>
    LoginResponseModel.fromJson(json.decode(str));

String loginResponseModelToJson(LoginResponseModel data) =>
    json.encode(data.toJson());

class LoginResponseModel {
  LoginResponseModel({
    this.username,
    this.roleName,
    this.roles,
  });

  String username;
  String roleName;
  List<int> roles;

  factory LoginResponseModel.fromJson(Map<String, dynamic> json) =>
      LoginResponseModel(
        username: json["username"],
        roleName: json["Role_Name"],
        roles: List<int>.from(json["roles"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "username": username,
        "Role_Name": roleName,
        "roles": List<dynamic>.from(roles.map((x) => x)),
      };
}
