part of 'pickingthree_cubit.dart';

@immutable
abstract class PickingthreeState {}

class PickingthreeInitial extends PickingthreeState {}

class PickingthreeList extends PickingthreeState {}

class PickingthreeError extends PickingthreeState {}
