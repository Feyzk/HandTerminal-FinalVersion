import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:deneme/base/consts/uri_consts.dart';
import 'package:deneme/views/Picking2/cubit/pickingtwo_cubit.dart';
import 'package:deneme/views/Picking2/model/active_order_list_response.dart';
import 'package:deneme/views/Picking2/model/picking_car_response.dart';
import 'package:deneme/views/Picking2/model/read_location_barcode_response.dart';
import 'package:deneme/views/Picking2/model/read_product_barcode.response.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

part 'pickingthree_state.dart';

class PickingthreeCubit extends Cubit<PickingthreeState> {
  PickingthreeCubit() : super(PickingthreeInitial());

  String storeName = "";
  String barcode = "";
  bool isValidProduct;
  bool isValidLocation;
  bool cannotFindStatus;
  bool pickingDone;

  int numberOfProducts = 0;
  bool locationBarcodeStatus;

  bool activeListStatus = true;
  List errors = ["badRequestError", "noCarError", "noActiveOrderError", "noBarcodeError"];

  void emitInitial() {
    emit(PickingthreeInitial());
  }

  // ignore: missing_return
  Future<ActiveOrderListResponse> activeOrderList({
    String username,
    String pickingOrderID,
    String pickingCarID,
  }) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/picking/order_list'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          'username': username,
          "Picking_Order_ID": pickingOrderID,
          "Picking_Car_ID": pickingCarID,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = ActiveOrderListResponse.fromJson(jsonDecode(response.body));
      this.storeName = jsonBody.storeName;
      this.barcode = jsonBody.barcode;
      emit(PickingthreeList());
    } else {
      this.storeName = "-";
      this.barcode = "-";
      this.activeListStatus = false;
      this.pickingDone = true;
    }
  }

  // ignore: missing_return
  Future<dynamic> readProductBarcode({
    String username,
    String pickingOrderID,
    String pickingCarID,
    String storeName,
    String locationBarcode,
    String barcode,
  }) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/picking/read_barcode'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          "username": username,
          "Picking_Order_ID": pickingOrderID,
          "Picking_Car_ID": pickingCarID,
          "Store_Name": storeName,
          "Location_Barcode": locationBarcode,
          "barcode": barcode
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = ReadProductBarcodeResponse.fromJson(
        jsonDecode(response.body),
      );
      isValidProduct = true;
      this.numberOfProducts = this.numberOfProducts + 1;
    } else {
      isValidProduct = false;
    }
  }

  Future<dynamic> cannotFind({
    String username,
    String pickingOrderId,
    String pickingCarId,
    String storeName,
    String locationBarcode,
    String barcode,
  }) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/picking/cannot_find'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          "username": username,
          "Picking_Order_ID": pickingOrderId,
          "Picking_Car_ID": pickingCarId,
          "Store_Name": storeName,
          "Location_Barcode": locationBarcode,
          "barcode": barcode
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      cannotFindStatus = true;
    } else {
      cannotFindStatus = false;
    }
  }
}
