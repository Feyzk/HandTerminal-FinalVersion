import 'package:deneme/base/base-view/base_view.dart';
import 'package:deneme/base/consts/route_consts.dart';
import 'package:deneme/base/consts/toast_message.dart';
import 'package:deneme/generated/l10n.dart';
import 'package:deneme/views/Picking3/cubit/pickingthree_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../base/extension/content_extension.dart';

class PickingThreeView extends StatefulWidget {
  final List arguments;
  const PickingThreeView({Key key, this.arguments}) : super(key: key);

  @override
  _PickingThreeViewState createState() => _PickingThreeViewState();
}

class _PickingThreeViewState extends State<PickingThreeView> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _textController;
  TextEditingController _textController2;
  TextEditingController _textController3;
  FocusNode myFocusNode;
  FocusNode myFocusNode2;
  ToastMessage toastMessage = ToastMessage();
  PickingthreeCubit pickingthreeCubit = PickingthreeCubit();

  @override
  void initState() {
    pickingthreeCubit.activeOrderList(
      username: widget.arguments[0][0][1],
      pickingOrderID: widget.arguments[0][1],
      pickingCarID: widget.arguments[1],
    );
    super.initState();
    myFocusNode = FocusNode();
    myFocusNode2 = FocusNode();
    _textController = TextEditingController();
    _textController2 = TextEditingController();
    _textController3 = TextEditingController();
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    myFocusNode2.dispose();
    _textController3.dispose();
    _textController2.dispose();
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final _pickingThreeCubit = BlocProvider.of<PickingthreeCubit>(context);

    return BaseView(
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios_outlined,
        ),
        onPressed: () {
          Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0][0]);
        },
      ),
      appBarTitle: delegate.picking,
      home: BlocConsumer<PickingthreeCubit, PickingthreeState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is PickingthreeInitial) {
            _pickingThreeCubit
                .activeOrderList(
                  username: widget.arguments[0][0][1],
                  pickingOrderID: widget.arguments[0][1],
                  pickingCarID: widget.arguments[1],
                )
                .whenComplete(() => {
                      if (_pickingThreeCubit.pickingDone == true)
                        {
                          Navigator.pushNamed(
                            context,
                            ALLOCATION,
                            arguments: widget.arguments,
                          )
                        }
                    });

            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is PickingthreeList) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
              child: Column(
                children: [
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: buildNumberText(_pickingThreeCubit, delegate),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: buildTextField(_pickingThreeCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: buildTextTwoField(_pickingThreeCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: buildLocationBarcodeField(delegate, _pickingThreeCubit, context),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: buildProductBarcodeField(_pickingThreeCubit, delegate, context),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: buildToAllocationButton(_pickingThreeCubit, context, delegate),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Expanded(
                    flex: 3,
                    child: buildCannotFindButton(_pickingThreeCubit, delegate),
                  ),
                  Spacer(
                    flex: 5,
                  ),
                ],
              ),
            );
          } else {
            return Center(
              child: Text('Emir Yok'),
            );
          }
        },
      ),
    );
  }

  Text buildNumberText(PickingthreeCubit _pickingThreeCubit, S delegate) => Text(
        '${delegate.numberOfTransactions + _pickingThreeCubit.numberOfProducts.toString()}',
        style: TextStyle(fontSize: 22),
      );

  Text buildTextField(PickingthreeCubit _pickingThreeCubit) {
    return Text(
      _pickingThreeCubit.storeName,
      style: TextStyle(fontSize: 22),
    );
  }

  Text buildTextTwoField(PickingthreeCubit _pickingThreeCubit) {
    return Text(
      _pickingThreeCubit.barcode,
      style: TextStyle(fontSize: 22),
    );
  }

  SizedBox buildToAllocationButton(PickingthreeCubit _pickingThreeCubit, BuildContext context, S delegate) {
    return SizedBox(
      width: context.dynamicWidth(0.8),
      height: context.dynamicHeigt(0.1),
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.black87),
        ),
        onPressed: _pickingThreeCubit.activeListStatus
            ? null
            : () => Navigator.pushNamed(
                  context,
                  ALLOCATION,
                  arguments: widget.arguments,
                ),
        child: Text(delegate.toAllocation),
      ),
    );
  }

  TextFormField buildLocationBarcodeField(S delegate, PickingthreeCubit _pickingThreeCubit, BuildContext context) {
    return TextFormField(
      controller: _textController2,
      focusNode: myFocusNode2,
      autofocus: true,
      onFieldSubmitted: (value) {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        if (value == null || value.isEmpty) {
          return Text(delegate.carBarcodeError);
        } else {
          myFocusNode.requestFocus();

          if (_pickingThreeCubit.storeName == _textController2.text) {
            myFocusNode.requestFocus();
          } else {
            FocusScope.of(context).requestFocus(FocusNode());
            toastMessage.showToast(context, delegate.locationBarcodeError);
          }
        }
      },
      decoration: InputDecoration(
        labelText: delegate.locationBarcode,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }

  TextFormField buildProductBarcodeField(
    PickingthreeCubit _pickingThreeCubit,
    S delegate,
    BuildContext context,
  ) {
    return TextFormField(
      focusNode: myFocusNode,
      onFieldSubmitted: (value) {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        _pickingThreeCubit
            .readProductBarcode(
              username: widget.arguments[0][0][1],
              pickingOrderID: widget.arguments[0][1],
              pickingCarID: widget.arguments[1],
              storeName: _pickingThreeCubit.storeName,
              locationBarcode: _textController2.text,
              barcode: _textController3.text,
            )
            .then(
              (response) => {
                if (_pickingThreeCubit.isValidProduct == true)
                  {
                    _pickingThreeCubit.activeOrderList(
                      username: widget.arguments[0][0][1],
                      pickingOrderID: widget.arguments[0][1],
                      pickingCarID: widget.arguments[1],
                    ),
                    _textController3.clear(),
                    _textController2.clear(),
                    myFocusNode2.requestFocus()
                  }
                else
                  {
                    FocusScope.of(context).requestFocus(FocusNode()),
                    toastMessage.showToast(context, delegate.productBarcodeError),
                  }
              },
            );
      },
      controller: _textController3,
      decoration: InputDecoration(
        labelText: delegate.productBarcode,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }

  SizedBox buildCannotFindButton(PickingthreeCubit _pickingThreeCubit, S delegate) {
    return SizedBox(
      width: context.dynamicWidth(0.8),
      height: context.dynamicHeigt(0.1),
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.black87),
        ),
        onPressed: () {
          _pickingThreeCubit
              .cannotFind(
            username: widget.arguments[0][0][1],
            pickingOrderId: widget.arguments[0][1],
            pickingCarId: widget.arguments[1],
            storeName: _pickingThreeCubit.storeName,
            locationBarcode: _textController2.text,
            barcode: _textController3.text,
          )
              .then((value) {
            if (_pickingThreeCubit.cannotFindStatus == true) {
              toastMessage.showToast(context, 'alternatif ürün oluşturuldu');
            } else {
              toastMessage.showToast(context, 'alternatif ürün bulunamadı');
            }
          }).then((value) {
            _pickingThreeCubit.activeOrderList(
              username: widget.arguments[0][0][1],
              pickingOrderID: widget.arguments[0][1],
              pickingCarID: widget.arguments[1],
            );
          });
        },
        child: Text(delegate.cannotFind),
      ),
    );
  }
}
