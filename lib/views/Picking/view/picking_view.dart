import 'package:deneme/base/consts/toast_message.dart';
import 'package:deneme/generated/intl/messages_tr_TR.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/route_consts.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/picking_cubit.dart';

class PickingView extends StatefulWidget {
  final List arguments;
  PickingView({
    Key key,
    this.arguments,
  }) : super(key: key);

  @override
  _PickingViewState createState() => _PickingViewState();
}

class _PickingViewState extends State<PickingView> {
  ToastMessage _toastMessage = ToastMessage();

  @override
  Widget build(BuildContext context) {
    final _pickingCubit = BlocProvider.of<PickingCubit>(context);
    final S delegate = S.of(context);
    _pickingCubit.orderList(username: widget.arguments[1]);
    return BaseView(
      username: widget.arguments[1],
      appBarTitle: delegate.picking,
      home: BlocConsumer<PickingCubit, PickingState>(
        listener: (context, state) {
          if (state is PickingTrue) {
            var orderID = state.orderPickResponse.pickingOrderId;
            Navigator.pushNamed(context, PICKING_TWO, arguments: [widget.arguments, orderID]);
          } else if (state is PickingThree) {
            Navigator.pushNamed(context, PICKING_THREE, arguments: [
              [widget.arguments, state.orderList[0].pickingOrderId],
              state.orderList[0].pickingCarId
            ]);
          }
        },
        // ignore: missing_return
        builder: (context, state) {
          if (state is PickingInitial) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is PickingOrderList) {
            return Padding(
              padding: EdgeInsets.symmetric(
                horizontal: context.dynamicWidth(0.05),
                vertical: context.dynamicWidth(0.1),
              ),
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(color: Colors.white60),
                itemCount: state.orderListResponse.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      var orderID = state.orderListResponse[index].pickingOrderId;
                      _pickingCubit.pickOrder(username: widget.arguments[1], orderID: orderID);
                    },
                    child: ListTile(
                      leading: Icon(Icons.arrow_forward_ios_outlined),
                      title: Text(state.orderListResponse[index].pickingOrderId),
                    ),
                  );
                },
              ),
            );
          } else if (state is PickingTrue) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is PickingError) {
            if (state.message == "noOrder") {
              return Center(
                child: Text(delegate.noOrderError),
              );
            } else {
              return Center(
                child: Text(delegate.badRequestError),
              );
            }
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
