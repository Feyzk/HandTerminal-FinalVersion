import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import '../../../base/consts/uri_consts.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../model/order_list_response_model.dart';
import '../model/order_pick_response_model.dart';

part 'picking_state.dart';

class PickingCubit extends Cubit<PickingState> {
  PickingCubit() : super(PickingInitial());

  List errors = ["badRequestError", "noOrderError"];
  String orderId;
  String carId;

  // ignore: missing_return
  Future<List<OrderListResponse>> orderList({String username}) async {
    final response = await http.post(Uri.http(UriConst.uri, 'api/picking/list_suitable'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, String>{'username': username},
        ));
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = (jsonDecode(response.body)) as List;
      var orderList = jsonBody.map((e) => OrderListResponse.fromJson(e)).toList();
      emit(PickingOrderList(orderList));
      if (orderList.isEmpty) {
        emit(PickingError("noOrder"));
      }
    } else if (response.statusCode == HttpStatus.notFound) {
      emit(PickingError("noOrder"));
    } else if (response.statusCode == HttpStatus.unauthorized) {
      var jsonBody = (jsonDecode(response.body)) as List;
      var orderList = jsonBody.map((e) => OrderListResponse.fromJson(e)).toList();
      emit(PickingThree(orderList));
    } else {
      emit(PickingError("badRequest"));
    }
  }

  // ignore: missing_return
  Future<OrderPickResponse> pickOrder({String username, String orderID}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/picking/select_order'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{'username': username, 'Picking_Order_ID': orderID}),
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = OrderPickResponse.fromJson(jsonDecode(response.body));
      this.orderId = jsonBody.pickingOrderId;
      emit(PickingTrue(jsonBody));
    } else {
      emit(PickingError('badRequestError'));
    }
  }
}
