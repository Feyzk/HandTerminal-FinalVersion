part of 'picking_cubit.dart';

@immutable
abstract class PickingState {}

class PickingInitial extends PickingState {}

class PickingOrderList extends PickingState {
  final List<OrderListResponse> orderListResponse;

  PickingOrderList(this.orderListResponse);
}

class PickingTrue extends PickingState {
  final OrderPickResponse orderPickResponse;

  PickingTrue(this.orderPickResponse);
}

class PickingFalse extends PickingState {
  final String message;

  PickingFalse(this.message);
}

class PickingError extends PickingState {
  final String message;

  PickingError(this.message);
}

class PickingThree extends PickingState {
  final List<OrderListResponse> orderList;

  PickingThree(this.orderList);
}
