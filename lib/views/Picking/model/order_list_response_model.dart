import 'dart:convert';

List<OrderListResponse> orderListResponseFromJson(String str) =>
    List<OrderListResponse>.from(json.decode(str).map((x) => OrderListResponse.fromJson(x)));

String orderListResponseToJson(List<OrderListResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OrderListResponse {
  OrderListResponse({
    this.pickingOrderId,
    this.pickingCarId,
  });

  String pickingOrderId;
  String pickingCarId;

  factory OrderListResponse.fromJson(Map<String, dynamic> json) => OrderListResponse(
        pickingOrderId: json["Picking_Order_ID"],
        pickingCarId: json["Picking_Car_ID"],
      );

  Map<String, dynamic> toJson() => {
        "Picking_Order_ID": pickingOrderId,
        "Picking_Car_ID": pickingCarId,
      };
}
