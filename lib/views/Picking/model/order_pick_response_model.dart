import 'dart:convert';

OrderPickResponse orderPickResponseFromJson(String str) =>
    OrderPickResponse.fromJson(json.decode(str));

String orderPickResponseToJson(OrderPickResponse data) =>
    json.encode(data.toJson());

class OrderPickResponse {
  OrderPickResponse({
    this.status,
    this.pickingOrderId,
    this.message,
  });

  bool status;
  String pickingOrderId;
  String message;

  factory OrderPickResponse.fromJson(Map<String, dynamic> json) =>
      OrderPickResponse(
        status: json["status"],
        pickingOrderId: json["Picking_Order_ID"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "Picking_Order_ID": pickingOrderId,
        "message": message,
      };
}
