import 'dart:convert';

AccResponseModel accResponseModelFromJson(String str) =>
    AccResponseModel.fromJson(json.decode(str));

String accResponseModelToJson(AccResponseModel data) =>
    json.encode(data.toJson());

class AccResponseModel {
  AccResponseModel({
    this.match,
    this.acceptanceId,
    this.invoiceNumber,
  });

  bool match;
  String acceptanceId;
  String invoiceNumber;

  factory AccResponseModel.fromJson(Map<String, dynamic> json) =>
      AccResponseModel(
        match: json["match"],
        acceptanceId: json["acceptance_id"],
        invoiceNumber: json["Invoice_Number"],
      );

  Map<String, dynamic> toJson() => {
        "match": match,
        "acceptance_id": acceptanceId,
        "Invoice_Number": invoiceNumber,
      };
}
