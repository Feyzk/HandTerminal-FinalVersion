import 'dart:convert';

InvoiceResponseModel invoiceResponseModelFromJson(String str) =>
    InvoiceResponseModel.fromJson(json.decode(str));

String invoiceResponseModelToJson(InvoiceResponseModel data) =>
    json.encode(data.toJson());

class InvoiceResponseModel {
  InvoiceResponseModel({
    this.message,
    this.acceptanceId,
    this.invoiceNumber,
  });

  String message;
  String acceptanceId;
  String invoiceNumber;

  factory InvoiceResponseModel.fromJson(Map<String, dynamic> json) =>
      InvoiceResponseModel(
        message: json["message"],
        acceptanceId: json["acceptance_id"],
        invoiceNumber: json["Invoice_Number"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "acceptance_id": acceptanceId,
        "Invoice_Number": invoiceNumber,
      };
}
