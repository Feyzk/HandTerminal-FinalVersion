// To parse this JSON data, do
//
//     final invoiceRequestModel = invoiceRequestModelFromJson(jsonString);

import 'dart:convert';

InvoiceRequestModel invoiceRequestModelFromJson(String str) =>
    InvoiceRequestModel.fromJson(json.decode(str));

String invoiceRequestModelToJson(InvoiceRequestModel data) =>
    json.encode(data.toJson());

class InvoiceRequestModel {
  InvoiceRequestModel({
    this.invoiceNumber,
    this.userName,
  });

  String invoiceNumber;
  String userName;

  factory InvoiceRequestModel.fromJson(Map<String, dynamic> json) =>
      InvoiceRequestModel(
        invoiceNumber: json["Invoice_Number"],
        userName: json["User_Name"],
      );

  Map<String, dynamic> toJson() => {
        "Invoice_Number": invoiceNumber,
        "User_Name": userName,
      };
}
