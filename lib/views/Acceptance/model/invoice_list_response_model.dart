// To parse this JSON data, do
//
//     final InvoiceListResponseModel = InvoiceListResponseModelFromJson(jsonString);

import 'dart:convert';

// ignore: non_constant_identifier_names
InvoiceListResponseModel InvoiceListResponseModelFromJson(String str) =>
    InvoiceListResponseModel.fromJson(json.decode(str));

// ignore: non_constant_identifier_names
String InvoiceListResponseModelToJson(InvoiceListResponseModel data) =>
    json.encode(data.toJson());

class InvoiceListResponseModel {
  InvoiceListResponseModel({
    this.invoiceId,
  });

  String invoiceId;

  factory InvoiceListResponseModel.fromJson(Map<String, dynamic> json) =>
      InvoiceListResponseModel(
        invoiceId: json["invoice_id"],
      );

  Map<String, dynamic> toJson() => {
        "invoice_id": invoiceId,
      };
}
