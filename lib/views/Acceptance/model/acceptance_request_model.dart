import 'dart:convert';

AccRequestModel accRequestModelFromJson(String str) =>
    AccRequestModel.fromJson(json.decode(str));

String accRequestModelToJson(AccRequestModel data) =>
    json.encode(data.toJson());

class AccRequestModel {
  AccRequestModel({
    this.username,
  });

  String username;

  factory AccRequestModel.fromJson(Map<String, dynamic> json) =>
      AccRequestModel(
        username: json["username"],
      );

  Map<String, dynamic> toJson() => {
        "username": username,
      };
}
