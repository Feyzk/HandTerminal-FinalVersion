import 'package:deneme/base/consts/toast_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/route_consts.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/acceptance_cubit.dart';

class AcceptanceViewScreen extends StatefulWidget {
  final List arguments;

  const AcceptanceViewScreen({Key key, this.arguments}) : super(key: key);
  @override
  _AcceptanceViewScreenState createState() => _AcceptanceViewScreenState();
}

class _AcceptanceViewScreenState extends State<AcceptanceViewScreen> {
  @override
  Widget build(BuildContext context) {
    ToastMessage toastMessage = ToastMessage();
    final _acceptanceCubit = BlocProvider.of<AcceptanceCubit>(context);
    final S delegate = S.of(context);
    _acceptanceCubit.acceptanceMatch(widget.arguments[1]);

    return BaseView(
      username: widget.arguments[1],
      appBarTitle: delegate.acceptance,
      home: BlocConsumer<AcceptanceCubit, AcceptanceState>(
        listener: (context, state) {
          if (state is AcceptanceTrue) {
            Navigator.of(context).pushNamedAndRemoveUntil(
              ACCEPTANCE_TWO_ROUTE,
              (route) => false,
              arguments: [
                widget.arguments,
                _acceptanceCubit.acceptanceID,
                _acceptanceCubit.invoiceID,
              ],
            );
          } else if (state is AcceptanceFalse) {
            _acceptanceCubit.invoiceList();
          } else if (state is AcceptanceInvoiceSelected) {
            Navigator.of(context).pushNamedAndRemoveUntil(
              ACCEPTANCE_TWO_ROUTE,
              (route) => false,
              arguments: [
                widget.arguments,
                _acceptanceCubit.acceptanceID,
                _acceptanceCubit.invoiceID,
              ],
            );
          } else if (state is AcceptanceError) {
            if (_acceptanceCubit.message == "1") {
              String message = delegate.invoiceReceived;
              toastMessage.showToast(context, message);
            } else {
              String message = delegate.badRequestError;
              toastMessage.showToast(context, message);
            }
          }
        },
        builder: (context, state) {
          if (state is AcceptanceInitial) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is AcceptanceList) {
            return Padding(
              padding: EdgeInsets.symmetric(
                horizontal: context.dynamicWidth(0.05),
                vertical: context.dynamicWidth(0.1),
              ),
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(color: Colors.white60),
                itemCount: state.invoiceListResponse.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      _acceptanceCubit.selectInvoice(username: widget.arguments[1], invoiceNumber: state.invoiceListResponse[index].invoiceId);
                    },
                    child: ListTile(
                      leading: Icon(Icons.arrow_forward_ios_outlined),
                      title: Text(
                        '${state.invoiceListResponse[index].invoiceId}',
                      ),
                    ),
                  );
                },
              ),
            );
          } else if (state is AcceptanceError) {
            String message;
            if (message == _acceptanceCubit.errors[0]) {
              message = delegate.badRequestError;
            } else {
              message = delegate.alreadySelected;
            }
            return Center(
              child: Text(message),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
/*
 */
