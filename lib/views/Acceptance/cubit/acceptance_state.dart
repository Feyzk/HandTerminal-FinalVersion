part of 'acceptance_cubit.dart';

@immutable
abstract class AcceptanceState {}

class AcceptanceInitial extends AcceptanceState {}

class AcceptanceTrue extends AcceptanceState {
  final AccResponseModel accResponseModel;

  AcceptanceTrue(this.accResponseModel);
}

class AcceptanceFalse extends AcceptanceState {}

class AcceptanceList extends AcceptanceState {
  final List<InvoiceListResponseModel> invoiceListResponse;

  AcceptanceList(this.invoiceListResponse);
}

class AcceptanceInvoiceSelected extends AcceptanceState {
  final InvoiceResponseModel invoiceResponseModel;

  AcceptanceInvoiceSelected(this.invoiceResponseModel);
}

class AcceptanceError extends AcceptanceState {
  final String message;

  AcceptanceError(this.message);
}
