import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:deneme/base/consts/toast_message.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';
import '../model/acceptance_response_model.dart';
import '../model/invoice_list_response_model.dart';
import '../model/invoice_response_model.dart';

part 'acceptance_state.dart';

class AcceptanceCubit extends Cubit<AcceptanceState> {
  AcceptanceCubit() : super(AcceptanceInitial());

  String acceptanceID;
  String invoiceID;
  String message = "";

  List errors = ["badRequestError", "alreadySelected", "invoiceReceived"];

  // ignore: missing_return
  Future<List<InvoiceListResponseModel>> invoiceList() async {
    final response = await http.get(Uri.http(UriConst.uri, 'api/acceptance/1/list_suitable'), headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    });
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = (jsonDecode(response.body)) as List;
      var invoiceList = jsonBody.map((e) => InvoiceListResponseModel.fromJson(e)).toList();
      emit(AcceptanceList(invoiceList));
    } else {
      emit(AcceptanceError(errors[0]));
    }
  }

  // ignore: missing_return
  Future<InvoiceResponseModel> selectInvoice({String username, String invoiceNumber}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/acceptance/1/select_invoice'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{'username': username, "Invoice_Number": invoiceNumber},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = InvoiceResponseModel.fromJson(jsonDecode(response.body));
      this.acceptanceID = responseBody.acceptanceId;
      this.invoiceID = responseBody.invoiceNumber;
      emit(AcceptanceInvoiceSelected(responseBody));
    } else if (response.statusCode == HttpStatus.unauthorized) {
      print(response.statusCode);
      this.message = "1";
      emit(AcceptanceError(errors[2]));
    } else {
      this.message = "2";
      print(response.statusCode);
      emit(AcceptanceError(errors[0]));
    }
  }

  // ignore: missing_return
  Future<AccResponseModel> acceptanceMatch(String username) async {
    final response = await http.post(Uri.http(UriConst.uri, 'api/acceptance/1/match_control'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{'username': username}));
    if (response.statusCode == HttpStatus.ok) {
      var accResponse = AccResponseModel.fromJson(jsonDecode(response.body));
      if (accResponse.match == true) {
        this.acceptanceID = accResponse.acceptanceId;
        this.invoiceID = accResponse.invoiceNumber;
        emit(AcceptanceTrue(accResponse));
      } else if (accResponse.match == false) {
        emit(AcceptanceFalse());
      } else {
        emit(AcceptanceError(errors[1]));
      }
    } else {
      emit(AcceptanceError(errors[0]));
    }
  }
}
