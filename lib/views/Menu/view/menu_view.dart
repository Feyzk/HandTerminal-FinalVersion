import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/route_consts.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/menu_cubit.dart';

class MenuView extends StatefulWidget {
  final List arguments;

  const MenuView({
    Key key,
    this.arguments,
  }) : super(key: key);
  @override
  _MenuViewState createState() => _MenuViewState();
}

class _MenuViewState extends State<MenuView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final List roles = widget.arguments[0];
    final List menuList = [
      delegate.acceptance,
      delegate.zifbag,
      delegate.shipmentBag,
      delegate.dummy,
      delegate.carriage,
      delegate.picking,
      delegate.shifting,
      delegate.counting,
    ];
    final List routes = [
      ACCEPTANCE_ROUTE,
      ZIFBAG,
      SHIPMENT_ONE,
      DUMMY_ROUTE,
      CARRIAGE,
      PICKING,
      SHIFTING,
      COUNTING,
    ];

    return BaseView(
      username: widget.arguments[1],
      appBarTitle: delegate.menu,
      finishLeading: Center(
        child: Text(
          ' ${widget.arguments[1]}',
          style: TextStyle(
            color: Colors.white,
            fontSize: 15,
            fontWeight: FontWeight.w800,
          ),
        ),
      ),
      home: BlocBuilder<MenuCubit, MenuState>(
        // ignore: missing_return
        builder: (context, state) {
          if (state is MenuInitial) {
            return Padding(
              padding: EdgeInsets.symmetric(
                horizontal: context.dynamicWidth(0.1),
                vertical: context.dynamicWidth(0.1),
              ),
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(color: Colors.white60),
                itemCount: menuList.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      if (roles[index] == 1) {
                        Navigator.pushNamed(context, routes[index], arguments: widget.arguments);
                      } else {
                        return null;
                      }
                    },
                    child: ListTile(
                      leading: Icon(Icons.arrow_forward_ios_outlined),
                      title: Text(
                        '${menuList[index]}',
                      ),
                    ),
                  );
                },
              ),
            );
          }
        },
      ),
    );
  }
}
