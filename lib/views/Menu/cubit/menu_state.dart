part of 'menu_cubit.dart';

@immutable
abstract class MenuState {}

class MenuInitial extends MenuState {}

class AcceptanceTrue extends MenuState {
  final AccResponseModel accResponseModel;

  AcceptanceTrue(this.accResponseModel);
}

class AcceptanceFalse extends MenuState {}

class AcceptanceError extends MenuState {
  final String message;

  AcceptanceError(this.message);
}
