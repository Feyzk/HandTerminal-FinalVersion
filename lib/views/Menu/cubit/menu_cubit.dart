import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../Acceptance/model/acceptance_response_model.dart';

part 'menu_state.dart';

class MenuCubit extends Cubit<MenuState> {
  MenuCubit() : super(MenuInitial());
}
