import 'package:deneme/base/base-view/base_view.dart';
import 'package:deneme/base/consts/route_consts.dart';
import 'package:deneme/base/consts/uri_consts.dart';
import 'package:deneme/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../base/extension/content_extension.dart';

class ChangeIpView extends StatefulWidget {
  const ChangeIpView({Key key}) : super(key: key);

  @override
  _ChangeIpViewState createState() => _ChangeIpViewState();
}

class _ChangeIpViewState extends State<ChangeIpView> {
  TextEditingController _controller;
  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    final delegate = S.of(context);
    return BaseView(
      appBarTitle: delegate.changeIp,
      home: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: context.dynamicWidth(0.1),
          vertical: context.dynamicWidth(0.1),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Spacer(
              flex: 5,
            ),
            Expanded(
              flex: 3,
              child: TextFormField(
                autofocus: true,
                controller: _controller,
                decoration: InputDecoration(
                  hintText: delegate.ipAddress,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ),
            Spacer(
              flex: 2,
            ),
            Expanded(
              flex: 2,
              child: SizedBox(
                width: context.dynamicWidth(0.8),
                height: context.dynamicHeigt(0.05),
                child: ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.black87)),
                  onPressed: () async {
                    final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                    if (_controller.text.isNotEmpty) {
                      await sharedPreferences.setString('ip', _controller.text);
                      UriConst.uri = sharedPreferences.getString('ip');
                    } else {
                      await sharedPreferences.setString('ip', '35.239.139.93:8282');
                      UriConst.uri = sharedPreferences.getString('ip');
                    }
                    UriConst.uri = sharedPreferences.getString('ip');
                    Navigator.pop(context);
                  },
                  child: Text(delegate.save),
                ),
              ),
            ),
            Spacer(
              flex: 9,
            )
          ],
        ),
      ),
    );
  }
}
