import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';
import '../model/read_bag_id_model.dart';

part 'shipment_state.dart';

class ShipmentCubit extends Cubit<ShipmentState> {
  ShipmentCubit() : super(ShipmentInitial());

  bool status;
  List errors = ["notBelongLocation", "bagHasOpened", "noStore", "closedBag", "badRequestError"];
  String correctBagId;
  double opacity = 1;

  void emitInitial() {
    emit(ShipmentInitial());
  }

  // ignore: missing_return
  Future<ReadBagIdResponse> getBagID({String username, String storeName, String bagID}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/shipment/read_bag'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username, "Store_Name": storeName, "Bag_Id": bagID},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = ReadBagIdResponse.fromJson(
        jsonDecode(response.body),
      );

      if (responseBody.status == "0") {
        emit(ShipmentOneCompleted(responseBody));
      } else if (responseBody.status == "1") {
        emit(ShipmentOneCompleted(responseBody));
      } else if (responseBody.status == "2") {
        correctBagId = responseBody.bagId;
        emit(ShipmentError(errors[0]));
      } else if (responseBody.status == "3") {
        emit(ShipmentError(errors[1]));
      } else if (responseBody.status == "4") {
        emit(ShipmentError(errors[2]));
      } else if (responseBody.status == "5") {
        emit(ShipmentError(errors[3]));
      } else if (responseBody.status == "6") {
        emit(ShipmentError(errors[4]));
      }
    }
  }
}
