part of 'shipment_cubit.dart';

@immutable
abstract class ShipmentState {}

class ShipmentInitial extends ShipmentState {}

class ShipmentError extends ShipmentState {
  final String message;

  ShipmentError(this.message);
}

class ShipmentOneCompleted extends ShipmentState {
  final ReadBagIdResponse readBagIdResponse;

  ShipmentOneCompleted(this.readBagIdResponse);
}
