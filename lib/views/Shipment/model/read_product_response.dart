import 'dart:convert';

ReadProductResponse readProductResponseFromJson(String str) =>
    ReadProductResponse.fromJson(json.decode(str));

String readProductResponseToJson(ReadProductResponse data) =>
    json.encode(data.toJson());

class ReadProductResponse {
  ReadProductResponse({
    this.dummy,
    this.message,
  });

  bool dummy;
  String message;

  factory ReadProductResponse.fromJson(Map<String, dynamic> json) =>
      ReadProductResponse(
        dummy: json["dummy"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "dummy": dummy,
        "message": message,
      };
}
