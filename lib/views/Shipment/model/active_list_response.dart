import 'dart:convert';

List<GetActiveListResponse> getActiveListResponseFromJson(String str) =>
    List<GetActiveListResponse>.from(json.decode(str).map((x) => GetActiveListResponse.fromJson(x)));

String getActiveListResponseToJson(List<GetActiveListResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetActiveListResponse {
  GetActiveListResponse({
    this.barcode,
    this.parameterResult,
  });

  String barcode;
  String parameterResult;

  factory GetActiveListResponse.fromJson(Map<String, dynamic> json) => GetActiveListResponse(
        barcode: json["Barcode"],
        parameterResult: json["parameter_result"],
      );

  Map<String, dynamic> toJson() => {
        "Barcode": barcode,
        "parameter_result": parameterResult,
      };
}
