import 'dart:convert';

ReadBagIdResponse readBagIdResponseFromJson(String str) =>
    ReadBagIdResponse.fromJson(json.decode(str));

String readBagIdResponseToJson(ReadBagIdResponse data) =>
    json.encode(data.toJson());

class ReadBagIdResponse {
  ReadBagIdResponse({
    this.status,
    this.message,
    this.bagId,
  });

  String status;
  String message;
  String bagId;

  factory ReadBagIdResponse.fromJson(Map<String, dynamic> json) =>
      ReadBagIdResponse(
        status: json["status"],
        message: json["message"],
        bagId: json["Bag_ID"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "Bag_ID": bagId,
      };
}
