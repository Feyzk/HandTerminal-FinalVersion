import 'dart:convert';

CloseBagResponse closeBagResponseFromJson(String str) =>
    CloseBagResponse.fromJson(json.decode(str));

String closeBagResponseToJson(CloseBagResponse data) =>
    json.encode(data.toJson());

class CloseBagResponse {
  CloseBagResponse({
    this.closed,
    this.message,
  });

  bool closed;
  String message;

  factory CloseBagResponse.fromJson(Map<String, dynamic> json) =>
      CloseBagResponse(
        closed: json["closed"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "closed": closed,
        "message": message,
      };
}
