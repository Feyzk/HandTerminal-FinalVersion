import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/route_consts.dart';
import '../../../base/consts/toast_message.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/shipment_cubit.dart';

class ShipmentView extends StatefulWidget {
  final List arguments;

  const ShipmentView({Key key, this.arguments}) : super(key: key);
  @override
  _ShipmentViewState createState() => _ShipmentViewState();
}

class _ShipmentViewState extends State<ShipmentView> {
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  TextEditingController _textController;
  TextEditingController _textController2;
  FocusNode _focusNode;

  ToastMessage toastMessage = ToastMessage();

  @override
  void initState() {
    _focusNode = FocusNode();
    _textController = TextEditingController();
    _textController2 = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _textController.dispose();
    _textController2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final _shipmentCubit = BlocProvider.of<ShipmentCubit>(context);
    return BaseView(
      username: widget.arguments[1],
      appBarTitle: delegate.shipmentBag,
      home: BlocConsumer<ShipmentCubit, ShipmentState>(
        listener: (context, state) {
          if (state is ShipmentOneCompleted) {
            String bagID = _textController2.text;
            String storeName = _textController.text;
            if (_shipmentCubit.status == false) {
              final String message = delegate.bagIdMessage;
              final snackBar = SnackBar(content: Text(message));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }

            Navigator.of(context).pushNamed(SHIPMENT_TWO, arguments: [widget.arguments, bagID, storeName]);
          } else if (state is ShipmentError) {
            String message = state.message;
            if (message == _shipmentCubit.errors[0]) {
              message = delegate.notBelongLocation;
            } else if (message == _shipmentCubit.errors[1]) {
              message = delegate.bagHasOpened;
            } else if (message == _shipmentCubit.errors[2]) {
              message = delegate.noStore;
            } else if (message == _shipmentCubit.errors[3]) {
              message = delegate.closedBag;
            } else {
              message = delegate.badRequestError;
            }
            FocusScope.of(context).requestFocus(FocusNode());
            toastMessage.showToast(context, message);
            _shipmentCubit.emitInitial();
          }
        },
        // ignore: missing_return
        builder: (context, state) {
          if (state is ShipmentInitial) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Spacer(
                    flex: 2,
                  ),
                  Expanded(
                    flex: 6,
                    child: buildStoreBarcodeField(delegate),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 6,
                    child: buildBagIdField(delegate, _shipmentCubit),
                  ),
                  Spacer(
                    flex: 3,
                  ),
                  Expanded(
                    flex: 2,
                    child: _shipmentCubit.correctBagId == null
                        ? Text("")
                        : Text(
                            'Correct Bag ID: ${_shipmentCubit.correctBagId}',
                            style: TextStyle(fontSize: 24),
                          ),
                  ),
                  Spacer(
                    flex: 10,
                  )
                ],
              ),
            );
          } else if (state is ShipmentOneCompleted) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is ShipmentError) {
            toastMessage.showToast(context, delegate.closedBag);
          }
        },
      ),
    );
  }

  Form buildBagIdField(S delegate, ShipmentCubit _shipmentCubit) {
    return Form(
      key: _formKey2,
      child: TextFormField(
        focusNode: _focusNode,
        controller: _textController2,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            return null;
          }
        },
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            _shipmentCubit.getBagID(username: widget.arguments[1], storeName: _textController.text, bagID: _textController2.text);
          }
        },
        decoration: InputDecoration(
          labelText: delegate.bagID,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  Form buildStoreBarcodeField(S delegate) {
    return Form(
      key: _formKey,
      child: TextFormField(
        autofocus: true,
        controller: _textController,
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          if (value == null || value.isEmpty) {
            return (delegate.storeBarcodeError);
          } else {
            _focusNode.requestFocus();
          }
        },
        validator: (value) {
          if (value == null || value.isEmpty) {
            return (delegate.storeBarcodeError);
          } else {
            return null;
          }
        },
        decoration: InputDecoration(
          labelText: delegate.storeName,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
