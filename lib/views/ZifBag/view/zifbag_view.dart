import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/toast_message.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/zifbag_cubit.dart';

class ZifbagView extends StatefulWidget {
  final List arguments;

  const ZifbagView({Key key, this.arguments}) : super(key: key);
  @override
  _ZifbagViewState createState() => _ZifbagViewState();
}

class _ZifbagViewState extends State<ZifbagView> {
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  TextEditingController _textController;
  TextEditingController _textController2;
  ToastMessage toastMessage = ToastMessage();
  FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
    _textController = TextEditingController();
    _textController2 = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    _focusNode.dispose();
    _textController.dispose();
    _textController2.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final _zifbagCubit = BlocProvider.of<ZifbagCubit>(context);
    return BaseView(
      appBarTitle: delegate.zifbag,
      username: widget.arguments[1],
      home: BlocBuilder<ZifbagCubit, ZifbagState>(
        builder: (context, state) {
          if (state is ZifbagInitial) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: buildBagIdField(delegate, _zifbagCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: buildStoreBarcodeField(delegate, _zifbagCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 10,
                    child: buildListView(_zifbagCubit),
                  )
                ],
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Form buildBagIdField(S delegate, ZifbagCubit _zifbagCubit) {
    return Form(
      key: _formKey2,
      child: TextFormField(
        autofocus: true,
        controller: _textController2,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            return null;
          }
        },
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            _zifbagCubit.checkBag(bagID: _textController2.text).then(
                  (response) => {
                    if (_zifbagCubit.checkBagStatus == true)
                      {
                        _focusNode.requestFocus(),
                      }
                    else
                      {
                        FocusScope.of(context).requestFocus(FocusNode()),
                        toastMessage.showToast(context, delegate.noBag),
                      }
                  },
                );
          }
        },
        decoration: InputDecoration(
          labelText: delegate.bagID,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  Form buildStoreBarcodeField(S delegate, ZifbagCubit _zifbagCubit) {
    return Form(
      key: _formKey,
      child: TextFormField(
        focusNode: _focusNode,
        controller: _textController,
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          if (value == null || value.isEmpty) {
            return (delegate.storeBarcodeError);
          } else {
            _zifbagCubit.readBag(username: widget.arguments[1], locationID: _textController.text, bagID: _textController2.text).then(
                  (response) => {
                    if (_zifbagCubit.bagStatus == false && _zifbagCubit.message == "0")
                      {FocusScope.of(context).requestFocus(FocusNode()), toastMessage.showToast(context, delegate.noBag)}
                    else if (_zifbagCubit.bagStatus == false && _zifbagCubit.message == "1")
                      {FocusScope.of(context).requestFocus(FocusNode()), toastMessage.showToast(context, delegate.wrongLocation)}
                    else if (_zifbagCubit.bagStatus == true)
                      {
                        _zifbagCubit.addBag(_textController2.text),
                        _zifbagCubit.addStore(_textController.text),
                      }
                  },
                );
          }
        },
        validator: (value) {
          if (value == null || value.isEmpty) {
            return (delegate.storeBarcodeError);
          } else {
            return null;
          }
        },
        decoration: InputDecoration(
          labelText: delegate.locationBarcode,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  ListView buildListView(ZifbagCubit _zifbagCubit) {
    return ListView.separated(
      itemBuilder: (context, index) {
        return ListTile(
          leading: Icon(Icons.arrow_forward_ios_outlined),
          title: Text(_zifbagCubit.bagList[index]),
          subtitle: Text(_zifbagCubit.storeList[index]),
        );
      },
      separatorBuilder: (context, index) => Divider(color: Colors.white60),
      itemCount: _zifbagCubit.bagList.length,
    );
  }
}
