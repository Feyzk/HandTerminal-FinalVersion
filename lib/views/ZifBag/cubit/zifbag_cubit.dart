import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';

part 'zifbag_state.dart';

class ZifbagCubit extends Cubit<ZifbagState> {
  ZifbagCubit() : super(ZifbagInitial());

  List bagList = [];

  List storeList = [];

  String message;

  bool checkBagStatus;

  bool bagStatus;

  void addBag(String bagID) {
    this.bagList.insert(0, bagID);
    emit(ZifbagInitial());
  }

  void addStore(String storeName) {
    this.storeList.insert(0, storeName);
    emit(ZifbagInitial());
  }

  Future<dynamic> checkBag({String bagID}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/zif/control_bag'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          "Bag_ID": bagID,
        },
      ),
    );

    if (response.statusCode == HttpStatus.ok) {
      this.checkBagStatus = true;
    } else if (response.statusCode == HttpStatus.notFound) {
      this.checkBagStatus = false;
    }
  }

  Future<dynamic> readBag({String username, String bagID, String locationID}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/zif/read_bag'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username, "Bag_ID": bagID, "Location_ID": locationID},
      ),
    );

    if (response.statusCode == HttpStatus.ok) {
      this.bagStatus = true;
      return true;
    } else if (response.statusCode == HttpStatus.notFound) {
      this.message = "0";
      this.bagStatus = false;
      return false;
    } else if (response.statusCode == HttpStatus.unauthorized) {
      this.message = "1";
      this.bagStatus = false;
      return false;
    }
  }
}
