part of 'zifbag_cubit.dart';

@immutable
abstract class ZifbagState {}

class ZifbagInitial extends ZifbagState {}

class ZifbagList extends ZifbagState {}

class ZifbagError extends ZifbagState {}
