import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';
import '../model/shift_response_model.dart';

part 'shifting_state.dart';

class ShiftingCubit extends Cubit<ShiftingState> {
  ShiftingCubit() : super(ShiftingInitial());

  String store1 = "";
  String store2 = "";
  String barcode = "";
  List productList = [];
  String locationMessage;
  String productMessage;
  String parameter;

  void addItem(String item) {
    productList.insert(0, item);
    emit(ShiftingInitial());
  }

  void deleteItem(int index) {
    productList.removeAt(index);
    emit(ShiftingInitial());
  }

  Future<ShiftResponse> shifting({
    String username,
    String locationBarcode,
    List productBarcode,
    String locationTwoBarcode,
  }) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/shifting/shift'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, dynamic>{
          'username': username,
          'location1_barcode': locationBarcode,
          'product_barcode': productBarcode,
          'location2_barcode': locationTwoBarcode
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = ShiftResponse.fromJson(
        jsonDecode(response.body),
      );
      this.store1 = responseBody.store1;
      this.store2 = responseBody.store2;
      this.barcode = responseBody.barcode;
      return responseBody;
    } else {}
  }

  Future<void> checkLocation({String location}) async {
    final response = await http.post(
      Uri.http(
        UriConst.uri,
        'api/shifting/location_control',
      ),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, dynamic>{
          'location_barcode': location,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      locationMessage = "3";
    } else if (response.statusCode == HttpStatus.notFound) {
      locationMessage = "1";
    } else {
      locationMessage = "2";
    }
  }

  Future<void> readProduct({String location, String barcode}) async {
    final response = await http.post(
      Uri.http(
        UriConst.uri,
        'api/shifting/read_barcode',
      ),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, dynamic>{'location1_barcode': location, 'product_barcode': barcode},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      productMessage = "3";
    } else if (response.statusCode == HttpStatus.unauthorized) {
      productMessage = "1";
    } else {
      productMessage = "2";
    }
  }

  Future<void> getParameter() async {
    final response = await http.get(
      Uri.http(UriConst.uri, '/api/shifting/parameter'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == HttpStatus.ok) {
      parameter = "1";
    } else {
      parameter = "0";
    }
  }
}
