import 'package:deneme/base/consts/route_consts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/toast_message.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/shifting_cubit.dart';

class ShiftingView extends StatefulWidget {
  final List arguments;

  const ShiftingView({Key key, this.arguments}) : super(key: key);
  @override
  _ShiftingViewState createState() => _ShiftingViewState();
}

class _ShiftingViewState extends State<ShiftingView> {
  TextEditingController _textController;
  TextEditingController _textController2;
  TextEditingController _textController3;

  FocusNode _focusNode;
  FocusNode _focusNode2;
  FocusNode _focusNode3;
  ToastMessage toastMessage = ToastMessage();

  @override
  void initState() {
    _textController = TextEditingController();
    _textController2 = TextEditingController();
    _textController3 = TextEditingController();
    _focusNode = FocusNode();
    _focusNode2 = FocusNode();
    _focusNode3 = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    _textController2.dispose();
    _textController3.dispose();
    _focusNode.dispose();
    _focusNode2.dispose();
    _focusNode3.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final _shiftingCubit = BlocProvider.of<ShiftingCubit>(context);
    _shiftingCubit.getParameter();
    return BaseView(
      username: widget.arguments[1],
      appBarTitle: delegate.shifting,
      finishLeading: TextButton(
        child: Text(delegate.shifting),
        onPressed: () {
          _shiftingCubit
              .shifting(
                  username: widget.arguments[1],
                  locationBarcode: _textController.text,
                  locationTwoBarcode: _textController2.text,
                  productBarcode: _shiftingCubit.productList)
              .then((response) => {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                      MENU_ROUTE,
                      (route) => false,
                      arguments: widget.arguments,
                    )
                  });
        },
      ),
      home: BlocBuilder<ShiftingCubit, ShiftingState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
            child: Form(
              child: Column(
                children: [
                  Expanded(
                    flex: 3,
                    child: buildLocationOneField(delegate, _shiftingCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: buildProductBarcodeField(delegate, _shiftingCubit, context),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: buildLocationTwoField(delegate, _shiftingCubit),
                  ),
                  Spacer(),
                  Expanded(
                    flex: 3,
                    child: Text(
                      _shiftingCubit.productList.length.toString(),
                      style: TextStyle(fontSize: 24),
                    ),
                  ),
                  Expanded(
                    flex: 12,
                    child: buildListView(_shiftingCubit),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  ListView buildListView(ShiftingCubit _shiftingCubit) {
    return ListView.separated(
      itemBuilder: (context, index) {
        if (_shiftingCubit.parameter == "1") {
          return Dismissible(
            onDismissed: (direction) {
              _shiftingCubit.deleteItem(index);
            },
            key: UniqueKey(),
            child: ListTile(
              title: Text(
                _shiftingCubit.productList[index],
              ),
            ),
          );
        } else {
          return ListTile(
            title: Text(
              _shiftingCubit.productList[index],
            ),
          );
        }
      },
      separatorBuilder: (context, index) => Divider(
        color: Colors.white60,
      ),
      itemCount: _shiftingCubit.productList.length,
    );
  }

  TextFormField buildLocationOneField(S delegate, ShiftingCubit _shiftingCubit) {
    return TextFormField(
      autofocus: true,
      focusNode: _focusNode,
      controller: _textController,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return delegate.barcodeError;
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
        labelText: delegate.locationOne,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
      ),
      onFieldSubmitted: (value) {
        _shiftingCubit.checkLocation(location: _textController.text).then(
              (_) => {
                if (_shiftingCubit.locationMessage == "1")
                  {
                    toastMessage.showToast(context, delegate.wrongLocation),
                    _textController.clear(),
                  }
                else if (_shiftingCubit.locationMessage == "2")
                  {
                    toastMessage.showToast(context, delegate.badRequestError),
                    _textController.clear(),
                  }
                else
                  {
                    SystemChannels.textInput.invokeMethod('TextInput.hide'),
                    _focusNode3.requestFocus(),
                  }
              },
            );
      },
    );
  }

  TextFormField buildLocationTwoField(
    S delegate,
    ShiftingCubit _shiftingCubit,
  ) {
    return TextFormField(
      focusNode: _focusNode2,
      controller: _textController2,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return delegate.barcodeError;
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
        labelText: delegate.locationTwo,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
      ),
      onFieldSubmitted: (value) {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        if (value == null || value.isEmpty) {
          return (Text(delegate.barcodeError));
        } else {
          _shiftingCubit.checkLocation(location: _textController2.text).then(
                (_) => {
                  if (_shiftingCubit.locationMessage == "1")
                    {
                      toastMessage.showToast(context, delegate.wrongLocation),
                      _textController2.clear(),
                    }
                  else if (_shiftingCubit.locationMessage == "2")
                    {
                      toastMessage.showToast(context, delegate.badRequestError),
                      _textController2.clear(),
                    }
                  else
                    {
                      SystemChannels.textInput.invokeMethod('TextInput.hide'),
                      FocusScope.of(context).requestFocus(FocusNode()),
                    }
                },
              );
        }
      },
    );
  }

  TextFormField buildProductBarcodeField(S delegate, ShiftingCubit _shiftingCubit, BuildContext context) {
    return TextFormField(
      focusNode: _focusNode3,
      controller: _textController3,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return delegate.barcodeError;
        } else {
          return null;
        }
      },
      onFieldSubmitted: (value) {
        _shiftingCubit.readProduct(location: _textController.text, barcode: value).then(
              (_) => {
                if (_shiftingCubit.productMessage == "1")
                  {
                    FocusScope.of(context).requestFocus(FocusNode()),
                    toastMessage.showToast(context, delegate.productBarcodeError),
                  }
                else if (_shiftingCubit.productMessage == "2")
                  {
                    FocusScope.of(context).requestFocus(FocusNode()),
                    toastMessage.showToast(context, delegate.badRequestError),
                  }
                else
                  {
                    SystemChannels.textInput.invokeMethod('TextInput.hide'),
                    _shiftingCubit.addItem(value),
                    _textController3.clear(),
                    _focusNode3.requestFocus(),
                  }
              },
            );
      },
      decoration: InputDecoration(
        labelText: delegate.productBarcode,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
      ),
    );
  }
}
