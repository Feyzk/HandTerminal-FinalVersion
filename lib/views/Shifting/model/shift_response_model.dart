import 'dart:convert';

ShiftResponse shiftResponseFromJson(String str) =>
    ShiftResponse.fromJson(json.decode(str));

String shiftResponseToJson(ShiftResponse data) => json.encode(data.toJson());

class ShiftResponse {
  ShiftResponse({
    this.status,
    this.message,
    this.store1,
    this.store2,
    this.barcode,
  });

  String status;
  String message;
  String store1;
  String store2;
  String barcode;

  factory ShiftResponse.fromJson(Map<String, dynamic> json) => ShiftResponse(
        status: json["status"],
        message: json["message"],
        store1: json["Store1"],
        store2: json["Store2"],
        barcode: json["barcode"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "Store1": store1,
        "Store2": store2,
        "barcode": barcode,
      };
}
