import 'dart:convert';

DummyBarcodeModel dummyBarcodeModelFromJson(String str) =>
    DummyBarcodeModel.fromJson(json.decode(str));

String dummyBarcodeModelToJson(DummyBarcodeModel data) =>
    json.encode(data.toJson());

class DummyBarcodeModel {
  DummyBarcodeModel({
    this.storeName,
  });

  String storeName;

  factory DummyBarcodeModel.fromJson(Map<String, dynamic> json) =>
      DummyBarcodeModel(
        storeName: json["Store_Name"],
      );

  Map<String, dynamic> toJson() => {
        "Store_Name": storeName,
      };
}
