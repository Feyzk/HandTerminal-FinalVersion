// To parse this JSON data, do
//
//     final dummyResponseModel = dummyResponseModelFromJson(jsonString);

import 'dart:convert';

DummyResponseModel dummyResponseModelFromJson(String str) =>
    DummyResponseModel.fromJson(json.decode(str));

String dummyResponseModelToJson(DummyResponseModel data) =>
    json.encode(data.toJson());

class DummyResponseModel {
  DummyResponseModel({
    this.status,
    this.message,
  });

  bool status;
  String message;

  factory DummyResponseModel.fromJson(Map<String, dynamic> json) =>
      DummyResponseModel(
        status: json["status"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
      };
}
