import 'package:deneme/base/consts/toast_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/route_consts.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/dummy_cubit.dart';

class DummyView extends StatefulWidget {
  final List arguments;

  const DummyView({Key key, this.arguments}) : super(key: key);
  @override
  _DummyViewState createState() => _DummyViewState();
}

class _DummyViewState extends State<DummyView> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _textController;
  FocusNode _focusNode;
  ToastMessage toastMessage = ToastMessage();

  @override
  void initState() {
    _textController = TextEditingController();
    _focusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final _dummyCubit = BlocProvider.of<DummyCubit>(context);
    _dummyCubit.statusControl();

    return BaseView(
      username: widget.arguments[1],
      appBarTitle: delegate.dummy,
      home: BlocConsumer<DummyCubit, DummyState>(
        listener: (context, state) {
          if (state is DummyInitial) {
            _dummyCubit.statusControl();
          } else if (state is DummyCompleted) {
            Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments);
          } else if (state is DummyStatusFalse) {
            final String message = delegate.dummyResponseError;
            toastMessage.showToast(context, message);
          }
        },
        // ignore: missing_return
        builder: (context, state) {
          if (state is DummyInitial) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is DummyStatusTrue) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    flex: 3,
                    child: buildDummyField(_dummyCubit, delegate),
                  ),
                  Expanded(
                    flex: 2,
                    child: buildStoreTextField(_dummyCubit),
                  ),
                  Expanded(
                    flex: 2,
                    child: buildFinishButton(_dummyCubit, delegate),
                  ),
                  Spacer(
                    flex: 2,
                  ),
                  Expanded(
                    flex: 15,
                    child: buildProductList(_dummyCubit),
                  ),
                ],
              ),
            );
          } else if (state is DummyError) {
            String message = state.message;
            if (state.message == "badRequestError") {
              message = delegate.badRequestError;
            } else {
              message = delegate.notCompletedError;
            }
            return Center(
              child: Text(message),
            );
          } else if (state is DummyStatusFalse) {
            return Center(
              child: Text(""),
            );
          } else if (state is DummyCompleted) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  ListView buildProductList(DummyCubit _dummyCubit) {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(color: Colors.white60),
      itemCount: _dummyCubit.productList.length,
      itemBuilder: (context, index) {
        return ListTile(
          leading: Icon(Icons.arrow_forward_ios_outlined),
          title: Text(_dummyCubit.productList[index]),
        );
      },
    );
  }

  SizedBox buildFinishButton(DummyCubit _dummyCubit, S delegate) {
    return SizedBox(
      width: context.dynamicWidth(0.8),
      height: context.dynamicHeigt(0.1),
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.black87),
        ),
        onPressed: () {
          _dummyCubit.dummyFinish(username: widget.arguments[1]);
        },
        child: Text(delegate.finish),
      ),
    );
  }

  SizedBox buildStoreTextField(DummyCubit _dummyCubit) {
    return SizedBox(
      width: context.dynamicWidth(0.05),
      height: context.dynamicHeigt(0.05),
      child: Column(
        children: [
          Text(
            _dummyCubit.storeName,
            style: TextStyle(fontSize: 20),
          )
        ],
      ),
    );
  }

  Form buildDummyField(DummyCubit _dummyCubit, S delegate) {
    return Form(
      key: _formKey,
      child: TextFormField(
        autofocus: true,
        focusNode: _focusNode,
        controller: _textController,
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          _focusNode.requestFocus();
          if (value == null || value.isEmpty) {
            return null;
          } else {
            _dummyCubit.storeName = "";
            _dummyCubit.readDummyBarcode(username: widget.arguments[1], barcode: value).then(
                  (_) => {
                    if (_dummyCubit.dummyStatus == true)
                      {_dummyCubit.addItem(value), _textController.clear()}
                    else
                      {toastMessage.showToast(context, '${delegate.noOpenBag} Store Name: ${_dummyCubit.errorStore}')}
                  },
                );
          }
        },
        validator: (value) {
          if (value == null || value.isEmpty) {
            return delegate.barcodeError;
          } else {
            return null;
          }
        },
        decoration: InputDecoration(
          labelText: delegate.productBarcode,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
