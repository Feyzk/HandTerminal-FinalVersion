part of 'dummy_cubit.dart';

@immutable
abstract class DummyState {}

class DummyInitial extends DummyState {}

class DummyStatusTrue extends DummyState {}

class DummyStatusFalse extends DummyState {
  final DummyResponseModel dummyResponseFalse;

  DummyStatusFalse(this.dummyResponseFalse);
}

class DummyError extends DummyState {
  final String message;

  DummyError(this.message);
}

class DummyCompleted extends DummyState {}
