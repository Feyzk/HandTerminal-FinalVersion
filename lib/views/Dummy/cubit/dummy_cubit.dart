import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';
import '../model/dummy_barcode_model.dart';
import '../model/dummy_response_model.dart';

part 'dummy_state.dart';

class DummyCubit extends Cubit<DummyState> {
  DummyCubit() : super(DummyInitial());

  List errors = ["badRequestError", "notCompletedError", "noOpenBag"];
  List productList = [];
  String storeName = "";
  String errorStore;
  bool dummyStatus;

  void addItem(String item) {
    productList.insert(0, item);
    emit(DummyStatusTrue());
  }

  // ignore: missing_return
  Future<DummyResponseModel> statusControl() async {
    final response = await http.get(
      Uri.http(
        UriConst.uri,
        'api/dummy/status_control',
      ),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == HttpStatus.ok) {
      DummyResponseModel responseBody = DummyResponseModel.fromJson(jsonDecode(response.body));
      if (responseBody.status == true) {
        emit(DummyStatusTrue());
      } else {
        emit(DummyStatusFalse(responseBody));
      }
    } else {
      emit(
        DummyError(
          errors[0],
        ),
      );
    }
  }

  // ignore: missing_return
  Future<DummyBarcodeModel> readDummyBarcode({String username, String barcode}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/dummy/read_barcode'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username, "barcode": barcode},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      dummyStatus = true;
      var responseBody = DummyBarcodeModel.fromJson(jsonDecode(response.body));
      this.storeName = responseBody.storeName;
    } else {
      dummyStatus = false;
      var responseBody = DummyBarcodeModel.fromJson(jsonDecode(response.body));
      this.errorStore = responseBody.storeName;
    }
  }

  Future<dynamic> dummyFinish({String username}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/dummy/finish'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      emit(DummyCompleted());
    } else if (response.statusCode == HttpStatus.notFound) {
      emit(DummyError(errors[2]));
    }
  }
}
