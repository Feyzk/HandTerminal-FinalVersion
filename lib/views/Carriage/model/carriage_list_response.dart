import 'dart:convert';

List<CarriageListResponse> carriageListResponseFromJson(String str) =>
    List<CarriageListResponse>.from(
        json.decode(str).map((x) => CarriageListResponse.fromJson(x)));

String carriageListResponseToJson(List<CarriageListResponse> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CarriageListResponse {
  CarriageListResponse({
    this.carriageOrderId,
  });

  String carriageOrderId;

  factory CarriageListResponse.fromJson(Map<String, dynamic> json) =>
      CarriageListResponse(
        carriageOrderId: json["Carriage_Order_ID"],
      );

  Map<String, dynamic> toJson() => {
        "Carriage_Order_ID": carriageOrderId,
      };
}
