import 'dart:convert';

CarriageMatchResponse carriageMatchResponseFromJson(String str) =>
    CarriageMatchResponse.fromJson(json.decode(str));

String carriageMatchResponseToJson(CarriageMatchResponse data) =>
    json.encode(data.toJson());

class CarriageMatchResponse {
  CarriageMatchResponse({
    this.status,
    this.carriageOrderId,
    this.truckPlate,
  });

  bool status;
  String carriageOrderId;
  String truckPlate;

  factory CarriageMatchResponse.fromJson(Map<String, dynamic> json) =>
      CarriageMatchResponse(
        status: json["status"],
        carriageOrderId: json["Carriage_Order_ID"],
        truckPlate: json["Truck_Plate"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "Carriage_Order_ID": carriageOrderId,
        "Truck_Plate": truckPlate,
      };
}
