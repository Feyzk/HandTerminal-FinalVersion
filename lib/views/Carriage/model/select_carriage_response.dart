import 'dart:convert';

SelectCarriageResponse selectCarriageResponseFromJson(String str) =>
    SelectCarriageResponse.fromJson(json.decode(str));

String selectCarriageResponseToJson(SelectCarriageResponse data) =>
    json.encode(data.toJson());

class SelectCarriageResponse {
  SelectCarriageResponse({
    this.status,
    this.carriageOrderId,
    this.truckPlate,
  });

  bool status;
  String carriageOrderId;
  String truckPlate;

  factory SelectCarriageResponse.fromJson(Map<String, dynamic> json) =>
      SelectCarriageResponse(
        status: json["status"],
        carriageOrderId: json["Carriage_Order_ID"],
        truckPlate: json["Truck_Plate"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "Carriage_Order_ID": carriageOrderId,
        "Truck_Plate": truckPlate,
      };
}
