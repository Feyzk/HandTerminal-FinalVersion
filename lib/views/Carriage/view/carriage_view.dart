import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/route_consts.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/carriage_cubit.dart';

class CarriageView extends StatefulWidget {
  final List arguments;

  const CarriageView({Key key, this.arguments}) : super(key: key);
  @override
  _CarriageViewState createState() => _CarriageViewState();
}

class _CarriageViewState extends State<CarriageView> {
  @override
  Widget build(BuildContext context) {
    final _carriageCubit = BlocProvider.of<CarriageCubit>(context);
    final S delegate = S.of(context);
    _carriageCubit.carriageAcceptanceMatchControl(username: widget.arguments[1]);
    return BaseView(
      username: widget.arguments[1],
      appBarTitle: delegate.carriage,
      home: BlocConsumer<CarriageCubit, CarriageState>(
        listener: (context, state) {
          if (state is CarriageMatchFalse) {
            String carriageOrderId = state.carriageMatchResponse.carriageOrderId;
            String truckPlate = state.carriageMatchResponse.truckPlate;
            Navigator.of(context).pushNamed(CARRIAGE_TWO, arguments: [widget.arguments, carriageOrderId, truckPlate]);
          } else if (state is CarriageMatchTrue) {
            _carriageCubit.getCarriageList();
          } else if (state is CarriageSelected) {
            String carriageOrderId = state.selectCarriageResponse.carriageOrderId;
            String truckPlate = state.selectCarriageResponse.truckPlate;
            Navigator.of(context).pushNamed(CARRIAGE_TWO, arguments: [widget.arguments, carriageOrderId, truckPlate]);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
        builder: (context, state) {
          if (state is CarriageInitial) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is CarriageList) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
              child: ListView.separated(
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        _carriageCubit.selectCarriage(
                            username: widget.arguments[1], carriageOrderId: state.carriageListResponse[index].carriageOrderId);
                      },
                      child: ListTile(
                        leading: Icon(Icons.arrow_forward_ios_outlined),
                        title: Text('${state.carriageListResponse[index].carriageOrderId}'),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) => Divider(
                        color: Colors.white60,
                      ),
                  itemCount: state.carriageListResponse.length),
            );
          } else if (state is CarriageError) {
            return Center(
              child: Text(delegate.badRequestError),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
