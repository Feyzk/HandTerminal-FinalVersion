part of 'carriage_cubit.dart';

@immutable
abstract class CarriageState {}

class CarriageInitial extends CarriageState {}

class CarriageMatchTrue extends CarriageState {}

class CarriageMatchFalse extends CarriageState {
  final CarriageMatchResponse carriageMatchResponse;

  CarriageMatchFalse(this.carriageMatchResponse);
}

class CarriageList extends CarriageState {
  final List<CarriageListResponse> carriageListResponse;

  CarriageList(this.carriageListResponse);
}

class CarriageSelected extends CarriageState {
  final SelectCarriageResponse selectCarriageResponse;

  CarriageSelected(this.selectCarriageResponse);
}

class CarriageError extends CarriageState {
  final String message;

  CarriageError(
    this.message,
  );
}
