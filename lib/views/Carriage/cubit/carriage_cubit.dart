import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';
import '../model/carriage_list_response.dart';
import '../model/carriage_match_response.dart';
import '../model/select_carriage_response.dart';

part 'carriage_state.dart';

class CarriageCubit extends Cubit<CarriageState> {
  CarriageCubit() : super(CarriageInitial());

  List errors = ["badRequestError"];
  String truckPlate;

  // ignore: missing_return
  Future<CarriageMatchResponse> carriageAcceptanceMatchControl({String username}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/carriage/match_control'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = CarriageMatchResponse.fromJson(
        jsonDecode(response.body),
      );
      if (jsonBody.status == true) {
        truckPlate = jsonBody.truckPlate;
        emit(CarriageMatchTrue());
      } else {
        emit(
          CarriageMatchFalse(jsonBody),
        );
      }
    } else {
      emit(CarriageError(errors[0]));
    }
  }

  // ignore: missing_return
  Future<CarriageListResponse> getCarriageList() async {
    final response = await http.get(Uri.http(UriConst.uri, 'api/carriage/list_suitable'), headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    });
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = (jsonDecode(response.body)) as List;
      var carriageList = jsonBody.map((e) => CarriageListResponse.fromJson(e)).toList();
      emit(
        CarriageList(carriageList),
      );
    } else {
      CarriageError(errors[0]);
    }
  }

  // ignore: missing_return
  Future<SelectCarriageResponse> selectCarriage({String username, String carriageOrderId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/carriage/select_order'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{"username": username, "Carriage_Order_ID": carriageOrderId},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = SelectCarriageResponse.fromJson(jsonDecode(response.body));
      truckPlate = jsonBody.truckPlate;
      emit(
        CarriageSelected(jsonBody),
      );
    } else {
      CarriageError(errors[0]);
    }
  }
}
