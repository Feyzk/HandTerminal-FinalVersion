part of 'counting_cubit.dart';

@immutable
abstract class CountingState {}

class CountingInitial extends CountingState {}

class CountingTrue extends CountingState {
  final CountingMatchResponse countingMatchResponse;

  CountingTrue(this.countingMatchResponse);
}

class CountingFalse extends CountingState {}

class CountingList extends CountingState {
  final List<CountingListResponse> countingListResponse;

  CountingList(this.countingListResponse);
}

class CountingInvoiceSelected extends CountingState {
  final String selectedCountingId;

  CountingInvoiceSelected(this.selectedCountingId);
}

class CountingError extends CountingState {
  final String message;

  CountingError(this.message);
}
