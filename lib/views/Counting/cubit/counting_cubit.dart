import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../base/consts/uri_consts.dart';
import '../model/counting_list_response.dart';
import '../model/counting_match_response.dart';
import '../model/select_counting_response.dart';

part 'counting_state.dart';

class CountingCubit extends Cubit<CountingState> {
  CountingCubit() : super(CountingInitial());

  String countMatchId;
  int productCount;
  List countingList = [];
  List errors = ["badRequestError", "tryAgainError"];

  Future<List<CountingListResponse>> getCountingList(String username) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/counting/list_counting'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          'username': username,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonBody = (jsonDecode(response.body)) as List;
      var invoiceList = jsonBody.map((e) => CountingListResponse.fromJson(e)).toList();
      countingList = invoiceList;
      productCount = invoiceList.length;
      emit(CountingList(invoiceList));
    } else {
      emit(CountingError(errors[0]));
    }
  }

  Future<dynamic> selectInvoice({String username, String countingId}) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/counting/select_counting'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{'username': username, 'Counting_ID': countingId},
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      emit(CountingInvoiceSelected(countingId));
    } else
      emit(
        CountingError(errors[0]),
      );
  }
}
