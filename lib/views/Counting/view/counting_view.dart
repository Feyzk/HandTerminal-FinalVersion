import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/route_consts.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/counting_cubit.dart';

class CountingView extends StatefulWidget {
  final List arguments;

  const CountingView({Key key, this.arguments}) : super(key: key);
  @override
  _CountingViewState createState() => _CountingViewState();
}

class _CountingViewState extends State<CountingView> {
  @override
  Widget build(BuildContext context) {
    final _countingCubit = BlocProvider.of<CountingCubit>(context);
    final S delegate = S.of(context);
    String countingId;
    _countingCubit.getCountingList(widget.arguments[1]);
    return BaseView(
      username: widget.arguments[1],
      appBarTitle: delegate.counting,
      home: BlocConsumer<CountingCubit, CountingState>(
        listener: (context, state) {
          if (state is CountingInvoiceSelected) {
            Navigator.of(context).pushNamed(COUNTING2, arguments: [widget.arguments, state.selectedCountingId]);
          }
        },
        builder: (context, state) {
          if (state is CountingInitial) {
            if (_countingCubit.productCount == 0) {
              return Center(
                child: Text('no product'),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          } else if (state is CountingList) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.05), vertical: context.dynamicWidth(0.1)),
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(color: Colors.white60),
                itemCount: _countingCubit.countingList.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      //countingId = _countingCubit.countingList[index];
                      _countingCubit.selectInvoice(
                        username: widget.arguments[1],
                        countingId: _countingCubit.countingList[index].countingId,
                      );
                    },
                    child: ListTile(
                      leading: Icon(Icons.arrow_forward_ios_outlined),
                      title: Text(
                        '${_countingCubit.countingList[index].countingId}',
                      ),
                    ),
                  );
                },
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
