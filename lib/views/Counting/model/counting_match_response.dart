import 'dart:convert';

CountingMatchResponse countingMatchResponseFromJson(String str) =>
    CountingMatchResponse.fromJson(json.decode(str));

String countingMatchResponseToJson(CountingMatchResponse data) =>
    json.encode(data.toJson());

class CountingMatchResponse {
  CountingMatchResponse({
    this.status,
    this.countingId,
  });

  bool status;
  String countingId;

  factory CountingMatchResponse.fromJson(Map<String, dynamic> json) =>
      CountingMatchResponse(
        status: json["status"],
        countingId: json["Counting_ID"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "Counting_ID": countingId,
      };
}
