// To parse this JSON data, do
//
//     final SelectCountingResponse = SelectCountingResponseFromJson(jsonString);

import 'dart:convert';

// ignore: non_constant_identifier_names
SelectCountingResponse SelectCountingResponseFromJson(String str) => SelectCountingResponse.fromJson(json.decode(str));

// ignore: non_constant_identifier_names
String SelectCountingResponseToJson(SelectCountingResponse data) => json.encode(data.toJson());

class SelectCountingResponse {
  SelectCountingResponse({
    this.countingID,
  });

  String countingID;

  factory SelectCountingResponse.fromJson(Map<String, dynamic> json) => SelectCountingResponse(
        countingID: json["Counting_ID"],
      );

  Map<String, dynamic> toJson() => {
        "Counting_ID": countingID,
      };
}
