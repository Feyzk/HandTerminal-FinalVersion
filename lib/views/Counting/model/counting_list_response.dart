import 'dart:convert';

List<CountingListResponse> countingListResponseFromJson(String str) =>
    List<CountingListResponse>.from(
        json.decode(str).map((x) => CountingListResponse.fromJson(x)));

String countingListResponseToJson(List<CountingListResponse> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CountingListResponse {
  CountingListResponse({
    this.countingId,
  });

  String countingId;

  factory CountingListResponse.fromJson(Map<String, dynamic> json) =>
      CountingListResponse(
        countingId: json["Counting_ID"],
      );

  Map<String, dynamic> toJson() => {
        "Counting_ID": countingId,
      };
}
