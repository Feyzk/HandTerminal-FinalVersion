part of 'pickingtwo_cubit.dart';

@immutable
abstract class PickingtwoState {}

class PickingtwoInitial extends PickingtwoState {}

class PickingtwoCarSelected extends PickingtwoState {
  final PickingCarResponse pickingCarResponse;

  PickingtwoCarSelected(this.pickingCarResponse);
}

class PickingtwoCarNotSelected extends PickingtwoState {}

class PickingtwoError extends PickingtwoState {
  final String message;

  PickingtwoError(this.message);
}
