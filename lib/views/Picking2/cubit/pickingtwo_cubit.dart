import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import '../../../base/consts/uri_consts.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../model/active_order_list_response.dart';
import '../model/picking_car_response.dart';
import '../model/read_location_barcode_response.dart';
import '../model/read_product_barcode.response.dart';

part 'pickingtwo_state.dart';

class PickingtwoCubit extends Cubit<PickingtwoState> {
  PickingtwoCubit() : super(PickingtwoInitial());

  String pickingCarID;

  // ignore: missing_return
  Future<PickingCarResponse> pickingCar({
    String username,
    String pickingOrderID,
    String pickingCarID,
  }) async {
    final response = await http.post(
      Uri.http(UriConst.uri, 'api/picking/select_picking_car'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          'username': username,
          "Picking_Order_ID": pickingOrderID,
          "Picking_Car_ID": pickingCarID,
        },
      ),
    );
    if (response.statusCode == HttpStatus.ok) {
      var responseBody = PickingCarResponse.fromJson(jsonDecode(response.body));
      this.pickingCarID = pickingCarID;
      emit(PickingtwoCarSelected(responseBody));
    } else if (response.statusCode == HttpStatus.badRequest) {
      emit(PickingtwoError("badRequest"));
    } else {
      emit(PickingtwoError("wrongCarIdMessage"));
    }
  }
}
