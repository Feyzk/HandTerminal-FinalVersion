import 'package:deneme/base/consts/route_consts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../base/base-view/base_view.dart';
import '../../../base/consts/toast_message.dart';
import '../../../base/extension/content_extension.dart';
import '../../../generated/l10n.dart';
import '../cubit/pickingtwo_cubit.dart';

class PickingTwoView extends StatefulWidget {
  final List arguments;

  const PickingTwoView({Key key, this.arguments}) : super(key: key);
  @override
  _PickingTwoViewState createState() => _PickingTwoViewState();
}

class _PickingTwoViewState extends State<PickingTwoView> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _textController;

  ToastMessage _toastMessage = ToastMessage();

  @override
  void initState() {
    _textController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final S delegate = S.of(context);
    final _pickingTwoCubit = BlocProvider.of<PickingtwoCubit>(context);

    return BaseView(
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios_outlined,
        ),
        onPressed: () {
          Navigator.of(context).pushNamedAndRemoveUntil(MENU_ROUTE, (route) => false, arguments: widget.arguments[0]);
        },
      ),
      appBarTitle: delegate.picking,
      home: BlocConsumer<PickingtwoCubit, PickingtwoState>(
        listener: (context, state) {
          if (state is PickingtwoCarSelected) {
            Navigator.of(context).pushNamed(PICKING_THREE, arguments: [
              widget.arguments,
              _textController.text,
            ]);
          } else if (state is PickingtwoError) {
            String message;
            if (state.message == "wrongCarIdMessage") {
              message = delegate.wrongCarIdMessage;
            } else {
              message = delegate.badRequestError;
            }
            FocusScope.of(context).requestFocus(FocusNode());
            _toastMessage.showToast(context, message);
          }
        },
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.1), vertical: context.dynamicWidth(0.1)),
            child: Column(
              children: [
                Form(
                  key: _formKey,
                  child: TextFormField(
                    controller: _textController,
                    autofocus: true,
                    onFieldSubmitted: (value) {
                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                      if (value == null || value.isEmpty) {
                        return Text(delegate.carBarcodeError);
                      } else {
                        _pickingTwoCubit.pickingCar(
                          username: widget.arguments[0][1],
                          pickingOrderID: widget.arguments[1],
                          pickingCarID: _textController.text,
                        );
                      }
                    },
                    decoration: InputDecoration(
                      labelText: delegate.carID,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
