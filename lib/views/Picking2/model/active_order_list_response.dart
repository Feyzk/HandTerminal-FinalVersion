import 'dart:convert';

ActiveOrderListResponse activeOrderListResponseFromJson(String str) =>
    ActiveOrderListResponse.fromJson(json.decode(str));

String activeOrderListResponseToJson(ActiveOrderListResponse data) =>
    json.encode(data.toJson());

class ActiveOrderListResponse {
  ActiveOrderListResponse({
    this.status,
    this.message,
    this.storeName,
    this.barcode,
  });

  bool status;
  String message;
  String storeName;
  String barcode;

  factory ActiveOrderListResponse.fromJson(Map<String, dynamic> json) =>
      ActiveOrderListResponse(
        status: json["status"],
        message: json["message"],
        storeName: json["Store_Name"],
        barcode: json["Barcode"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "Store_Name": storeName,
        "Barcode": barcode,
      };
}
