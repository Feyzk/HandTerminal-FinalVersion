import 'dart:convert';

PickingCarResponse pickingCarResponseFromJson(String str) =>
    PickingCarResponse.fromJson(json.decode(str));

String pickingCarResponseToJson(PickingCarResponse data) =>
    json.encode(data.toJson());

class PickingCarResponse {
  PickingCarResponse({
    this.status,
    this.pickingCarId,
    this.pickingCar,
  });

  bool status;
  String pickingCarId;
  String pickingCar;

  factory PickingCarResponse.fromJson(Map<String, dynamic> json) =>
      PickingCarResponse(
        status: json["status"],
        pickingCarId: json["Picking_Car_ID"],
        pickingCar: json["Picking_Car"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "Picking_Car_ID": pickingCarId,
        "Picking_Car": pickingCar,
      };
}
