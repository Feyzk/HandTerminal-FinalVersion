import 'dart:convert';

ReadProductBarcodeResponse readProductBarcodeResponseFromJson(String str) =>
    ReadProductBarcodeResponse.fromJson(json.decode(str));

String readProductBarcodeResponseToJson(ReadProductBarcodeResponse data) =>
    json.encode(data.toJson());

class ReadProductBarcodeResponse {
  ReadProductBarcodeResponse({
    this.read,
    this.message,
  });

  bool read;
  String message;

  factory ReadProductBarcodeResponse.fromJson(Map<String, dynamic> json) =>
      ReadProductBarcodeResponse(
        read: json["read"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "read": read,
        "message": message,
      };
}
