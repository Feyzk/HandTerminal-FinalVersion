import 'dart:convert';

ReadLocationBarcodeResponse readLocationBarcodeResponseFromJson(String str) =>
    ReadLocationBarcodeResponse.fromJson(json.decode(str));

String readLocationBarcodeResponseToJson(ReadLocationBarcodeResponse data) =>
    json.encode(data.toJson());

class ReadLocationBarcodeResponse {
  ReadLocationBarcodeResponse({
    this.status,
    this.locationBarcode,
    this.message,
  });

  bool status;
  String locationBarcode;
  String message;

  factory ReadLocationBarcodeResponse.fromJson(Map<String, dynamic> json) =>
      ReadLocationBarcodeResponse(
        status: json["status"],
        locationBarcode: json["Location_Barcode"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "Location_Barcode": locationBarcode,
        "message": message,
      };
}
