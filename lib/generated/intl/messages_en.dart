// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "acceptance" : MessageLookupByLibrary.simpleMessage("Acceptance"),
    "allocation" : MessageLookupByLibrary.simpleMessage("Allocation"),
    "allocationError" : MessageLookupByLibrary.simpleMessage("This product is not installed in the car"),
    "alreadyOnlineError" : MessageLookupByLibrary.simpleMessage("User is already online."),
    "alreadySelected" : MessageLookupByLibrary.simpleMessage("Acceptance already selected by another user."),
    "back" : MessageLookupByLibrary.simpleMessage("Back"),
    "badRequestError" : MessageLookupByLibrary.simpleMessage("Server error."),
    "bagHasOpened" : MessageLookupByLibrary.simpleMessage("Bag has been opened in another store for this barcode."),
    "bagID" : MessageLookupByLibrary.simpleMessage("BagID"),
    "bagIdMessage" : MessageLookupByLibrary.simpleMessage("There is such a bag available"),
    "barcodeError" : MessageLookupByLibrary.simpleMessage("Please enter barcode number."),
    "cannotFind" : MessageLookupByLibrary.simpleMessage("Can\'t Find"),
    "carBarcodeError" : MessageLookupByLibrary.simpleMessage("Please enter car barcode number."),
    "carID" : MessageLookupByLibrary.simpleMessage("CarID"),
    "carriage" : MessageLookupByLibrary.simpleMessage("Carriage"),
    "changeIp" : MessageLookupByLibrary.simpleMessage("Change Ip Address"),
    "closeBag" : MessageLookupByLibrary.simpleMessage("Close Bag"),
    "closedBag" : MessageLookupByLibrary.simpleMessage("This bag is close."),
    "counting" : MessageLookupByLibrary.simpleMessage("Counting"),
    "countingClosed" : MessageLookupByLibrary.simpleMessage("Count order has already been closed."),
    "dummy" : MessageLookupByLibrary.simpleMessage("Dummy"),
    "dummyResponseError" : MessageLookupByLibrary.simpleMessage("There is an invoice in the transaction."),
    "enterPassword" : MessageLookupByLibrary.simpleMessage("Please enter password."),
    "enterUsername" : MessageLookupByLibrary.simpleMessage("Please enter username."),
    "error" : MessageLookupByLibrary.simpleMessage("Error"),
    "finish" : MessageLookupByLibrary.simpleMessage("Finish"),
    "incompatible" : MessageLookupByLibrary.simpleMessage("Location type incompatible"),
    "internetError" : MessageLookupByLibrary.simpleMessage("Please check your internet connection."),
    "invoiceReceived" : MessageLookupByLibrary.simpleMessage("This invoice was received by someone else"),
    "ipAddress" : MessageLookupByLibrary.simpleMessage("Ip Address"),
    "locationBarcode" : MessageLookupByLibrary.simpleMessage("LocationBarcode"),
    "locationBarcodeError" : MessageLookupByLibrary.simpleMessage("Please check location barcode"),
    "locationID" : MessageLookupByLibrary.simpleMessage("Location"),
    "locationOne" : MessageLookupByLibrary.simpleMessage("LocationOne"),
    "locationTwo" : MessageLookupByLibrary.simpleMessage("LocationTwo"),
    "login" : MessageLookupByLibrary.simpleMessage("Login"),
    "logout" : MessageLookupByLibrary.simpleMessage("Logout"),
    "menu" : MessageLookupByLibrary.simpleMessage("Menu"),
    "name" : MessageLookupByLibrary.simpleMessage("Name"),
    "noActiveOrderError" : MessageLookupByLibrary.simpleMessage("Not found active order list."),
    "noBag" : MessageLookupByLibrary.simpleMessage("There is no such bag"),
    "noBarcodeError" : MessageLookupByLibrary.simpleMessage("Please enter a valid barcode."),
    "noCarError" : MessageLookupByLibrary.simpleMessage("You need to choose a car to trade."),
    "noLocation" : MessageLookupByLibrary.simpleMessage("Location not found."),
    "noOpenBag" : MessageLookupByLibrary.simpleMessage("There is no open bag for this store"),
    "noOrderError" : MessageLookupByLibrary.simpleMessage("There is no such order."),
    "noStore" : MessageLookupByLibrary.simpleMessage("There is no such store."),
    "notActive" : MessageLookupByLibrary.simpleMessage("Location is not active."),
    "notBelongLocation" : MessageLookupByLibrary.simpleMessage("The read bag does not belong to this location. Find the bag with the given barcode"),
    "notCompletedError" : MessageLookupByLibrary.simpleMessage("The process is not completed."),
    "notFoundError" : MessageLookupByLibrary.simpleMessage("Invalid username or password."),
    "notFoundLocationOne" : MessageLookupByLibrary.simpleMessage("No product found in first location!"),
    "notFoundLocationTwo" : MessageLookupByLibrary.simpleMessage("Second location not found"),
    "notLoadedCar" : MessageLookupByLibrary.simpleMessage("The product is not loaded in the car."),
    "notUserandCount" : MessageLookupByLibrary.simpleMessage("No user match with count found."),
    "numberOfTransactions" : MessageLookupByLibrary.simpleMessage("Number of transactions:"),
    "openedBag" : MessageLookupByLibrary.simpleMessage("There is open bag for this store."),
    "password" : MessageLookupByLibrary.simpleMessage("Password"),
    "picking" : MessageLookupByLibrary.simpleMessage("Picking"),
    "pickingCorrectMessage" : MessageLookupByLibrary.simpleMessage("You can take the products from the box."),
    "pickingWrongMessage" : MessageLookupByLibrary.simpleMessage("The location is not correct."),
    "preAcceptance" : MessageLookupByLibrary.simpleMessage("Pre-Acceptance"),
    "productBarcode" : MessageLookupByLibrary.simpleMessage("ProductBarcode"),
    "productBarcodeError" : MessageLookupByLibrary.simpleMessage("Please check product barcode"),
    "save" : MessageLookupByLibrary.simpleMessage("Save"),
    "shifting" : MessageLookupByLibrary.simpleMessage("Shifting"),
    "shipmentBag" : MessageLookupByLibrary.simpleMessage("Shipment Bag"),
    "shipmentFalseMessage" : MessageLookupByLibrary.simpleMessage("Throw the product into the bag."),
    "shipmentTrueMessage" : MessageLookupByLibrary.simpleMessage("Throw the product in the dummy box."),
    "storeBarcodeError" : MessageLookupByLibrary.simpleMessage("Please enter store barcode number."),
    "storeName" : MessageLookupByLibrary.simpleMessage("Store Name"),
    "success" : MessageLookupByLibrary.simpleMessage("Success!"),
    "toAllocation" : MessageLookupByLibrary.simpleMessage("To Allocation"),
    "tryAgainError" : MessageLookupByLibrary.simpleMessage("Something went wrong, try again later."),
    "undefinedUser" : MessageLookupByLibrary.simpleMessage("Undefined user"),
    "wrongBag" : MessageLookupByLibrary.simpleMessage("Wrong bag"),
    "wrongCarIdMessage" : MessageLookupByLibrary.simpleMessage("No cars with this barcode found."),
    "wrongLocation" : MessageLookupByLibrary.simpleMessage("Wrong location barcode"),
    "zifbag" : MessageLookupByLibrary.simpleMessage("Zif Bag")
  };
}
