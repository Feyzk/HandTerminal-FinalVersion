// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get logout {
    return Intl.message(
      'Logout',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name {
    return Intl.message(
      'Name',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Please enter username.`
  String get enterUsername {
    return Intl.message(
      'Please enter username.',
      name: 'enterUsername',
      desc: '',
      args: [],
    );
  }

  /// `Please enter password.`
  String get enterPassword {
    return Intl.message(
      'Please enter password.',
      name: 'enterPassword',
      desc: '',
      args: [],
    );
  }

  /// `Back`
  String get back {
    return Intl.message(
      'Back',
      name: 'back',
      desc: '',
      args: [],
    );
  }

  /// `Finish`
  String get finish {
    return Intl.message(
      'Finish',
      name: 'finish',
      desc: '',
      args: [],
    );
  }

  /// `CarID`
  String get carID {
    return Intl.message(
      'CarID',
      name: 'carID',
      desc: '',
      args: [],
    );
  }

  /// `BagID`
  String get bagID {
    return Intl.message(
      'BagID',
      name: 'bagID',
      desc: '',
      args: [],
    );
  }

  /// `ProductBarcode`
  String get productBarcode {
    return Intl.message(
      'ProductBarcode',
      name: 'productBarcode',
      desc: '',
      args: [],
    );
  }

  /// `LocationBarcode`
  String get locationBarcode {
    return Intl.message(
      'LocationBarcode',
      name: 'locationBarcode',
      desc: '',
      args: [],
    );
  }

  /// `To Allocation`
  String get toAllocation {
    return Intl.message(
      'To Allocation',
      name: 'toAllocation',
      desc: '',
      args: [],
    );
  }

  /// `Store Name`
  String get storeName {
    return Intl.message(
      'Store Name',
      name: 'storeName',
      desc: '',
      args: [],
    );
  }

  /// `Location`
  String get locationID {
    return Intl.message(
      'Location',
      name: 'locationID',
      desc: '',
      args: [],
    );
  }

  /// `Close Bag`
  String get closeBag {
    return Intl.message(
      'Close Bag',
      name: 'closeBag',
      desc: '',
      args: [],
    );
  }

  /// `Can't Find`
  String get cannotFind {
    return Intl.message(
      'Can\'t Find',
      name: 'cannotFind',
      desc: '',
      args: [],
    );
  }

  /// `LocationOne`
  String get locationOne {
    return Intl.message(
      'LocationOne',
      name: 'locationOne',
      desc: '',
      args: [],
    );
  }

  /// `LocationTwo`
  String get locationTwo {
    return Intl.message(
      'LocationTwo',
      name: 'locationTwo',
      desc: '',
      args: [],
    );
  }

  /// `Menu`
  String get menu {
    return Intl.message(
      'Menu',
      name: 'menu',
      desc: '',
      args: [],
    );
  }

  /// `Acceptance`
  String get acceptance {
    return Intl.message(
      'Acceptance',
      name: 'acceptance',
      desc: '',
      args: [],
    );
  }

  /// `Carriage`
  String get carriage {
    return Intl.message(
      'Carriage',
      name: 'carriage',
      desc: '',
      args: [],
    );
  }

  /// `Pre-Acceptance`
  String get preAcceptance {
    return Intl.message(
      'Pre-Acceptance',
      name: 'preAcceptance',
      desc: '',
      args: [],
    );
  }

  /// `Counting`
  String get counting {
    return Intl.message(
      'Counting',
      name: 'counting',
      desc: '',
      args: [],
    );
  }

  /// `Shifting`
  String get shifting {
    return Intl.message(
      'Shifting',
      name: 'shifting',
      desc: '',
      args: [],
    );
  }

  /// `Allocation`
  String get allocation {
    return Intl.message(
      'Allocation',
      name: 'allocation',
      desc: '',
      args: [],
    );
  }

  /// `Shipment Bag`
  String get shipmentBag {
    return Intl.message(
      'Shipment Bag',
      name: 'shipmentBag',
      desc: '',
      args: [],
    );
  }

  /// `Picking`
  String get picking {
    return Intl.message(
      'Picking',
      name: 'picking',
      desc: '',
      args: [],
    );
  }

  /// `Dummy`
  String get dummy {
    return Intl.message(
      'Dummy',
      name: 'dummy',
      desc: '',
      args: [],
    );
  }

  /// `Success!`
  String get success {
    return Intl.message(
      'Success!',
      name: 'success',
      desc: '',
      args: [],
    );
  }

  /// `Zif Bag`
  String get zifbag {
    return Intl.message(
      'Zif Bag',
      name: 'zifbag',
      desc: '',
      args: [],
    );
  }

  /// `Undefined user`
  String get undefinedUser {
    return Intl.message(
      'Undefined user',
      name: 'undefinedUser',
      desc: '',
      args: [],
    );
  }

  /// `Number of transactions:`
  String get numberOfTransactions {
    return Intl.message(
      'Number of transactions:',
      name: 'numberOfTransactions',
      desc: '',
      args: [],
    );
  }

  /// `No product found in first location!`
  String get notFoundLocationOne {
    return Intl.message(
      'No product found in first location!',
      name: 'notFoundLocationOne',
      desc: '',
      args: [],
    );
  }

  /// `Second location not found`
  String get notFoundLocationTwo {
    return Intl.message(
      'Second location not found',
      name: 'notFoundLocationTwo',
      desc: '',
      args: [],
    );
  }

  /// `This product is not installed in the car`
  String get allocationError {
    return Intl.message(
      'This product is not installed in the car',
      name: 'allocationError',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get error {
    return Intl.message(
      'Error',
      name: 'error',
      desc: '',
      args: [],
    );
  }

  /// `Please check location barcode`
  String get locationBarcodeError {
    return Intl.message(
      'Please check location barcode',
      name: 'locationBarcodeError',
      desc: '',
      args: [],
    );
  }

  /// `Please check product barcode`
  String get productBarcodeError {
    return Intl.message(
      'Please check product barcode',
      name: 'productBarcodeError',
      desc: '',
      args: [],
    );
  }

  /// `Please enter barcode number.`
  String get barcodeError {
    return Intl.message(
      'Please enter barcode number.',
      name: 'barcodeError',
      desc: '',
      args: [],
    );
  }

  /// `Please check your internet connection.`
  String get internetError {
    return Intl.message(
      'Please check your internet connection.',
      name: 'internetError',
      desc: '',
      args: [],
    );
  }

  /// `Invalid username or password.`
  String get notFoundError {
    return Intl.message(
      'Invalid username or password.',
      name: 'notFoundError',
      desc: '',
      args: [],
    );
  }

  /// `Server error.`
  String get badRequestError {
    return Intl.message(
      'Server error.',
      name: 'badRequestError',
      desc: '',
      args: [],
    );
  }

  /// `User is already online.`
  String get alreadyOnlineError {
    return Intl.message(
      'User is already online.',
      name: 'alreadyOnlineError',
      desc: '',
      args: [],
    );
  }

  /// `Something went wrong, try again later.`
  String get tryAgainError {
    return Intl.message(
      'Something went wrong, try again later.',
      name: 'tryAgainError',
      desc: '',
      args: [],
    );
  }

  /// `The process is not completed.`
  String get notCompletedError {
    return Intl.message(
      'The process is not completed.',
      name: 'notCompletedError',
      desc: '',
      args: [],
    );
  }

  /// `There is an invoice in the transaction.`
  String get dummyResponseError {
    return Intl.message(
      'There is an invoice in the transaction.',
      name: 'dummyResponseError',
      desc: '',
      args: [],
    );
  }

  /// `There is no such order.`
  String get noOrderError {
    return Intl.message(
      'There is no such order.',
      name: 'noOrderError',
      desc: '',
      args: [],
    );
  }

  /// `You need to choose a car to trade.`
  String get noCarError {
    return Intl.message(
      'You need to choose a car to trade.',
      name: 'noCarError',
      desc: '',
      args: [],
    );
  }

  /// `Not found active order list.`
  String get noActiveOrderError {
    return Intl.message(
      'Not found active order list.',
      name: 'noActiveOrderError',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a valid barcode.`
  String get noBarcodeError {
    return Intl.message(
      'Please enter a valid barcode.',
      name: 'noBarcodeError',
      desc: '',
      args: [],
    );
  }

  /// `Please enter car barcode number.`
  String get carBarcodeError {
    return Intl.message(
      'Please enter car barcode number.',
      name: 'carBarcodeError',
      desc: '',
      args: [],
    );
  }

  /// `Please enter store barcode number.`
  String get storeBarcodeError {
    return Intl.message(
      'Please enter store barcode number.',
      name: 'storeBarcodeError',
      desc: '',
      args: [],
    );
  }

  /// `Location is not active.`
  String get notActive {
    return Intl.message(
      'Location is not active.',
      name: 'notActive',
      desc: '',
      args: [],
    );
  }

  /// `Location not found.`
  String get noLocation {
    return Intl.message(
      'Location not found.',
      name: 'noLocation',
      desc: '',
      args: [],
    );
  }

  /// `You can take the products from the box.`
  String get pickingCorrectMessage {
    return Intl.message(
      'You can take the products from the box.',
      name: 'pickingCorrectMessage',
      desc: '',
      args: [],
    );
  }

  /// `The location is not correct.`
  String get pickingWrongMessage {
    return Intl.message(
      'The location is not correct.',
      name: 'pickingWrongMessage',
      desc: '',
      args: [],
    );
  }

  /// `There is such a bag available`
  String get bagIdMessage {
    return Intl.message(
      'There is such a bag available',
      name: 'bagIdMessage',
      desc: '',
      args: [],
    );
  }

  /// `Throw the product in the dummy box.`
  String get shipmentTrueMessage {
    return Intl.message(
      'Throw the product in the dummy box.',
      name: 'shipmentTrueMessage',
      desc: '',
      args: [],
    );
  }

  /// `Throw the product into the bag.`
  String get shipmentFalseMessage {
    return Intl.message(
      'Throw the product into the bag.',
      name: 'shipmentFalseMessage',
      desc: '',
      args: [],
    );
  }

  /// `No cars with this barcode found.`
  String get wrongCarIdMessage {
    return Intl.message(
      'No cars with this barcode found.',
      name: 'wrongCarIdMessage',
      desc: '',
      args: [],
    );
  }

  /// `This bag is close.`
  String get closedBag {
    return Intl.message(
      'This bag is close.',
      name: 'closedBag',
      desc: '',
      args: [],
    );
  }

  /// `There is open bag for this store.`
  String get openedBag {
    return Intl.message(
      'There is open bag for this store.',
      name: 'openedBag',
      desc: '',
      args: [],
    );
  }

  /// `Bag has been opened in another store for this barcode.`
  String get bagHasOpened {
    return Intl.message(
      'Bag has been opened in another store for this barcode.',
      name: 'bagHasOpened',
      desc: '',
      args: [],
    );
  }

  /// `There is no such store.`
  String get noStore {
    return Intl.message(
      'There is no such store.',
      name: 'noStore',
      desc: '',
      args: [],
    );
  }

  /// `There is no such bag`
  String get noBag {
    return Intl.message(
      'There is no such bag',
      name: 'noBag',
      desc: '',
      args: [],
    );
  }

  /// `Wrong bag`
  String get wrongBag {
    return Intl.message(
      'Wrong bag',
      name: 'wrongBag',
      desc: '',
      args: [],
    );
  }

  /// `No user match with count found.`
  String get notUserandCount {
    return Intl.message(
      'No user match with count found.',
      name: 'notUserandCount',
      desc: '',
      args: [],
    );
  }

  /// `Count order has already been closed.`
  String get countingClosed {
    return Intl.message(
      'Count order has already been closed.',
      name: 'countingClosed',
      desc: '',
      args: [],
    );
  }

  /// `Location type incompatible`
  String get incompatible {
    return Intl.message(
      'Location type incompatible',
      name: 'incompatible',
      desc: '',
      args: [],
    );
  }

  /// `Wrong location barcode`
  String get wrongLocation {
    return Intl.message(
      'Wrong location barcode',
      name: 'wrongLocation',
      desc: '',
      args: [],
    );
  }

  /// `There is no open bag for this store`
  String get noOpenBag {
    return Intl.message(
      'There is no open bag for this store',
      name: 'noOpenBag',
      desc: '',
      args: [],
    );
  }

  /// `The read bag does not belong to this location. Find the bag with the given barcode`
  String get notBelongLocation {
    return Intl.message(
      'The read bag does not belong to this location. Find the bag with the given barcode',
      name: 'notBelongLocation',
      desc: '',
      args: [],
    );
  }

  /// `This invoice was received by someone else`
  String get invoiceReceived {
    return Intl.message(
      'This invoice was received by someone else',
      name: 'invoiceReceived',
      desc: '',
      args: [],
    );
  }

  /// `Change Ip Address`
  String get changeIp {
    return Intl.message(
      'Change Ip Address',
      name: 'changeIp',
      desc: '',
      args: [],
    );
  }

  /// `Ip Address`
  String get ipAddress {
    return Intl.message(
      'Ip Address',
      name: 'ipAddress',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save {
    return Intl.message(
      'Save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `The product is not loaded in the car.`
  String get notLoadedCar {
    return Intl.message(
      'The product is not loaded in the car.',
      name: 'notLoadedCar',
      desc: '',
      args: [],
    );
  }

  /// `Acceptance already selected by another user.`
  String get alreadySelected {
    return Intl.message(
      'Acceptance already selected by another user.',
      name: 'alreadySelected',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'tr', countryCode: 'TR'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}